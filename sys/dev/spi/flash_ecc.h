#ifndef __FLASH_ECC_H__
#define __FLASH_ECC_H__
void flash_ECCCalculate(const unsigned char *data,unsigned char *ecc);
int flash_ECCCorrect(unsigned char *data, unsigned char *read_ecc, const unsigned char *test_ecc);
#endif
