/*
 * =====================================================================================
 * spi_m25p128.c
 *
 * Description:
 *  
 *
 * Copyright (c) 2007 National Research Center for Intelligent Computing
 * Systems. ALL RIGHTS RESERVED. See "COPYRIGHT" for detail
 *
 * Created:  2010年08月03日 00时08分51秒
 * Revision:  none
 * Compiler:  gcc
 *
 * Author:  JiangTao (), jiangtao@ncic.ac.cn
 * Company:  CAR Group, NCIC,ICT,CAS.
 *
 *spi_m25p128驱动
 *
 *作者：江涛 日期：2010/08/23
 *
 *修改文件：
 *sys/dev/spi/spi_m25p128.c	spi和m25p128驱动主文件
 *pmon/cmds/cmd_spi_flash.c	spi flash命令主文件
 *pmon/cmds/load.c			load命令主文件
 *pmon/fs/devfs.c				PMON设备文件系统，增加对spi flash的访问控制
 *Targets/Bonito3amcp68/conf/files.Bonito3amcp68	编译配置文件
 *
 *
 *新增加命令：
 *bootflash	从flash启动linux内核
 *flash_be	整片擦除flash
 *flash_se	flash擦除命令，每次擦除1个页面（256字节）
 *flash_w		烧写flash
 *flash_id	读取flash的ID
 *reset_flash	重新设置spi的频率，相位等参数
 *load -p	-r	烧写linux内核到flash的512字节处，内核的size写到flash的0字节处。
 *
 *
 *bootflash	从flash启动linux内核
 *	linux内核存放于flash的512字节位置。第1个页面的前4字节用于存放内核的size信息。因为PMON的设备读写必须是512字节对齐，所以内核放在512字节。
 *	参数：linux启动的命令行参数
 *	示例：bootflash root=/....
 *
 *flash_be	整片擦除flash
 *	参数：无
 *	示例：flash_be
 *	
 *flash_se	flash擦除命令，每次擦除1个页面（256字节）
 *	参数：-s 擦除的大小；-f 擦除的起始地址
 *	示例：flash_se -f 0 -s 256
 *	注意：擦除操作的起始地址和大小必须以256字节对齐，如果参数不对齐，将起始地址向下圆整到256，将size和offset相加并向上圆整到256。原则：包含想要擦除区间。
 *	
 *flash_w		烧写flash
 *	参数：-s 大小；-f 烧写的起始地址；-o 烧写文件的内存地址
 *	示例：flash_w -f 512 -s 0x600000 -o 0x81000000
 *
 *flash_id	读取flash的id
 *	参数：无
 *	示例：	flash_id
 *	
 *reset_flash	重新设置spi参数
 *	参数：-m 模式（0 cpol=cpha=0; 1 cpol=0,cpha=1; 2 cpol=1,cpha=0; 3 cpol=cpha=1;）-d spi分频系数（原始频率来自PCI 33MHz）
 *	示例：reset_flash -m 0 -d 2
 *	
 *load -p	-r	烧写内核到flash的512字节处，此处内核文件必须为elf格式
 *	参数：load命令的所有参数
 *	示例：load -p -r tftp://10.10.102.6/vmlinux
 *
 * TODO:
 *
 * =====================================================================================
 */

#include <sys/types.h>
#include <sys/param.h>
#include <sys/systm.h>
#include <sys/kernel.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/buf.h>
#include <sys/uio.h>
#include <sys/malloc.h>
#include <sys/errno.h>
#include <sys/device.h>
#include <sys/disklabel.h>
#include <sys/disk.h>
#include <sys/queue.h>
#include <pmon.h>
#include "flash_ecc.h"

#define M25P128_PAGE_SIZE    256
#define ECC_BASE            0xfD0000 //SAVE ECC for every page(256B). 3Bytes per page
#define M25P128_SIZE    0x1000000

#define SPI_REG_BASE 0xbfe001f0	// 3A

#define FCR_SPCR        0x00
#define SPE_SHIFT   6
#define MSTR_SHIFT   4
#define CPOL_SHIFT   3
#define CPHA_SHIFT   2
#define SPR_SHIFT   0
#define SPE   (1<<SPE_SHIFT)
#define MSTR   (1<<MSTR_SHIFT)
#define FCR_SPSR        0x01
#define FCR_SPDR        0x02
#define FCR_SPER        0x03

#define WREN    0x06
#define WRDI    0x04
#define RDID    0x9F
#define RDSR    0x05
#define WRSR    0x01
#define READ    0x03
#define FAST_READ    0x0B
#define PP    0x02
#define SE    0xD8
#define BE    0xC7

#define KSEG1_STORE8(addr, value)   *(volatile unsigned char *)((addr)) = ((unsigned char)value & 0xff)
#define KSEG1_LOAD8(addr)   *(volatile unsigned char *)((addr))
#define SET_SPI(idx,value) KSEG1_STORE8(SPI_REG_BASE+idx, value)
#define GET_SPI(idx)	get_spi(idx)

extern int SPI_FLASH_INITD;
static int m25p128_match __P ((struct device *, void *, void *));
static void m25p128_attach __P ((struct device *, struct device *, void *));
void m25p128_init (int , int);
struct m25p128_softc
{
    struct device sc_dev;
    int temp;
};
struct cfattach spi_flash_ca = {
    sizeof (struct m25p128_softc), m25p128_match, m25p128_attach
};

struct cfdriver spi_flash_cd = {
    NULL, "spi_flash", DV_DULL
};
    static int
m25p128_match (parent, match, aux)
    struct device *parent;
#if defined(__BROKEN_INDIRECT_CONFIG) || defined(__OpenBSD__)
    void *match;
#else
    struct cfdata *match;
#endif
    void *aux;
{
    printf ("I am m25p128\n");
    return (1);
}

//set gpio 2 output for godson3a
    void
gpio_cs_init (void)
{
    *(volatile unsigned char *) (0xbfe00120) =
        0xB & *(volatile unsigned char *) (0xbfe00120);
}

//set gpio output value for godson3a
    inline void
set_cs (int bit)
{
    if (bit)
        *(volatile unsigned char *) (0xbfe0011c) = 0x4;
    else
        *(volatile unsigned char *) (0xbfe0011c) = 0;
}

    static void
m25p128_attach (struct device *parent, struct device *self, void *aux)
{
    gpio_cs_init ();
    delay(1);
    set_cs (1);
    m25p128_init (3, 2);
}
    static unsigned char
get_spi (int idx)
{
    KSEG1_LOAD8 (SPI_REG_BASE + idx);
    return KSEG1_LOAD8 (SPI_REG_BASE);
}

static unsigned char m25p128_buf[512 * 100]
__attribute__ ((section ("data"), aligned (512)));
static unsigned char tbuf[256]
__attribute__ ((section ("data")));

    static inline unsigned char
flash_writeb_cmd (unsigned char value)
{
    unsigned char ret;

    SET_SPI (FCR_SPDR, value);
    while (GET_SPI (FCR_SPSR) & 0x01)
    {
        //printf("flash_writeb_cmd while loop\n");
    }

    ret = GET_SPI (FCR_SPDR);
    return ret;
}
    static inline unsigned char
flash_read_data (void)
{
    unsigned char ret;

    SET_SPI (FCR_SPDR, 0x00);
    while (GET_SPI (FCR_SPSR) & 0x01)
    {
        //printf("flash_writeb_cmd while loop\n");
    }

    ret = GET_SPI (FCR_SPDR);
    return ret;
    //return flash_writeb_cmd (0);
}

void m25p128_readid (void)
{
    unsigned char vendor_id;
    unsigned char mem_type;
    unsigned char mem_size;
    int i;

    set_cs (0);
    flash_writeb_cmd (RDID);
    vendor_id = flash_read_data ();
    //  for(i=0; i<20; i++)
    //    printf("id[%d] = 0x%x\n", i, flash_read_data ());
    mem_type = flash_read_data ();
    //  for(i=0; i<10; i++)
    //    printf("mem_type = 0x%x\n", get_spi(2));
    mem_size = flash_read_data ();
    //  for(i=0; i<10; i++)
    //    printf("mem_size = 0x%x\n", get_spi(2));
    printf ("the vendor_id=0x%x,the mem_type=0x%x, the mem_size=0x%x\n",
            vendor_id, mem_type, mem_size);
    set_cs (1);

    if(vendor_id == 0x20 && mem_type == 0x20 && mem_size == 0x18)
        SPI_FLASH_INITD = 1; 
}

void
m25p128_init (int mode, int div)
{
    int spcr, sper, cpol, cpha, spr, spre;
    printf ("Enter m25p128_init(), mode = %d, div = %d\n", mode, div);

    SET_SPI (FCR_SPCR, 0x10);	//reset default value
    SET_SPI (FCR_SPSR, 0xc0);	// clear sr
    //printf("%s: %d\n", __FILE__, __LINE__);
    switch (div)
    {
        case 2:
            spr = spre = 0;
            break;
        case 4:
            spre = 0;
            spr = 1;
            break;
        case 16:
            spre = 0;
            spr = 2;
            break;
        case 32:
            spre = 0;
            spr = 3;
            break;
        case 8:
            spre = 1;
            spr = 0;
            break;
        case 64:
            spre = 1;
            spr = 1;
            break;
        case 128:
            spre = 1;
            spr = 2;
            break;
        case 256:
            spre = 1;
            spr = 3;
            break;
        case 512:
            spre = 2;
            spr = 0;
            break;
        case 1024:
            spre = 2;
            spr = 1;
            break;
        case 2048:
            spre = 2;
            spr = 2;
            break;
        case 4096:
            spre = 2;
            spr = 3;
            break;
        default:
            spr = spre = 0;
            break;
    }
    switch (mode)
    {
        case 0:
            cpol = cpha = 0;
            break;
        case 1:
            cpol = 0;
            cpha = 1;
            break;
        case 2:
            cpol = 1;
            cpha = 0;
            break;
        case 3:
            cpol = cpha = 1;
            break;
        default:
            cpol = cpha = 0;
            break;
    }
    spcr = SPE | MSTR | (cpha << CPHA_SHIFT) | (cpol << CPOL_SHIFT) | (spr << SPR_SHIFT);
    sper = spre;
    SET_SPI (FCR_SPER, sper);
    SET_SPI (FCR_SPCR, spcr);	//00
    //dummy read to align read/write pointer
    GET_SPI (FCR_SPDR);
    GET_SPI (FCR_SPDR);
    GET_SPI (FCR_SPDR);

    m25p128_readid ();
}

//busy wait for 1ms
    static inline int
m25p128_busy_wait_1ms (void)
{
    int timeout = 1000;
    int ret = 0;
    int i;
    set_cs (0);
    flash_writeb_cmd (RDSR);
    ret = flash_read_data ();
    while ((ret & 1) && --timeout)	//wait WIP
    {
        delay(1);
        ret = flash_read_data ();
//        printf("RDSR = 0x%x\n", ret);
    }
    set_cs (1);
    return timeout ? 0 : 1;
}


    static int
mmin (int a, int b)
{
    return a < b ? a : b;
}
int m25p128_slow_read_ecc (int offset, void *buffer, size_t n)
{
    unsigned int pos = offset;
    unsigned char *buf = (unsigned char *) buffer;
    unsigned char *mbuf;
    int i, small, eccResult;
    size_t left = n;
    unsigned char calcEcc[3];
    unsigned char rEcc[3];
    int fixed = 0;
    int unfixed = 0;

    while(left)
    {
        mbuf = buf;
        small = mmin (left, M25P128_PAGE_SIZE);
        if(small < M25P128_PAGE_SIZE)
        {
            mbuf = tbuf;
        }
        m25p128_slow_read_noecc(pos, mbuf, M25P128_PAGE_SIZE);
        flash_ECCCalculate(mbuf, calcEcc);
        m25p128_slow_read_noecc(ECC_BASE + pos / M25P128_PAGE_SIZE * 3, rEcc, 3);
        eccResult = flash_ECCCorrect (mbuf, rEcc, calcEcc);
        if(eccResult > 0)
        {
            printf("ecc error fix performed on chunk 0x%x\n", pos);
        }
        if(eccResult < 0)
        {
            printf("ecc error unfixed on chunk 0x%x\n", pos);
            printf("read again");
            if(++unfixed == 5)
                return 0;
            continue;
        }
        if(small < M25P128_PAGE_SIZE)
        {
            for(i = 0; i < small; i++)
                buf[i] = mbuf[i];
        }
        left -= small;
        pos += small;
        buf += small;

    }
    return (n);

}

int m25p128_slow_read_noecc (int offset, void *buffer, size_t n)
{
    unsigned int pos = offset;
    unsigned char *buf = (unsigned char *) buffer;
    int i;

    if(!SPI_FLASH_INITD)
        return 0;
    delay(1);
    set_cs (0);
    flash_writeb_cmd (READ);
    flash_writeb_cmd ((pos >> 16) & 0xff);
    flash_writeb_cmd ((pos >> 8) & 0xff);
    flash_writeb_cmd (pos & 0xff);
    for (i = 0; i < n; i++)
        buf[i] = flash_read_data ();
    set_cs (1);

    dotik (3000, 0);
    return (n);

}

int m25p128_fast_read_ecc (int offset, void *buffer, size_t n)
{
    unsigned int pos = offset;
    unsigned char *buf = (unsigned char *) buffer;
    unsigned char *mbuf;
    int i, small, eccResult;
    size_t left = n;
    unsigned char calcEcc[3];
    unsigned char rEcc[3];
    int fixed = 0;
    int unfixed = 0;

    while(left)
    {
        mbuf = buf;
        small = mmin (left, M25P128_PAGE_SIZE);
        if(small < M25P128_PAGE_SIZE)
        {
            mbuf = tbuf;
        }
        m25p128_fast_read_noecc(pos, mbuf, M25P128_PAGE_SIZE);
        flash_ECCCalculate(mbuf, calcEcc);
        m25p128_fast_read_noecc(ECC_BASE + pos / M25P128_PAGE_SIZE * 3, rEcc, 3);
        eccResult = flash_ECCCorrect (mbuf, rEcc, calcEcc);
        if(eccResult > 0)
        {
            printf("ecc error fix performed on chunk 0x%x\n", pos);
            fixed++;
        }
        if(eccResult < 0)
        {
            printf("ecc error unfixed on chunk 0x%x\n", pos);
            printf("read again");
            if(++unfixed == 5)
                return 0;
            continue;
        }
        if(small < M25P128_PAGE_SIZE)
        {
            for(i = 0; i < small; i++)
                buf[i] = mbuf[i];
        }
        left -= small;
        pos += small;
        buf += small;

    }
    return (n);

}
    static int
m25p128_fast_read_noecc (int offset, void *buffer, size_t n)
{
    unsigned int pos = offset;
    unsigned char *buf = (unsigned char *) buffer;
    int i;

    if(!SPI_FLASH_INITD)
        return 0;
    delay(1);
    set_cs (0);
    flash_writeb_cmd (FAST_READ);
    flash_writeb_cmd ((pos >> 16) & 0xff);
    flash_writeb_cmd ((pos >> 8) & 0xff);
    flash_writeb_cmd (pos & 0xff);

    flash_writeb_cmd (0);  //dummy read
    for (i = 0; i < n; i++)
        buf[i] = flash_read_data ();
    set_cs (1);

    dotik (3000, 0);
    return (n);

}

int m25p128_read0(int offset, void *buffer, size_t n)
{
    if(offset >= ECC_BASE || (offset + n) >= ECC_BASE)
    {
        printf("flash size overflow\n");
        return 0;
    }
//    return m25p128_slow_read_noecc (offset, buffer, n);
//    return m25p128_fast_read_noecc (offset, buffer, n);
//    return m25p128_slow_read_ecc (offset, buffer, n);
    return m25p128_fast_read_ecc (offset, buffer, n);
}
int m25p128_write_ecc (int offset, void *buffer, size_t n)
{
    unsigned int pos = offset;
    unsigned char *buf = (unsigned char *) buffer;
    unsigned char *mbuf;
    int left = n;
    int i, small;
    unsigned char calcEcc[3];

    printf ("write ===pos: %d\n", pos);
    if(offset % 512 != 0)
    {
        printf ("addr must align 512\n");
        return 0;
    }
    while (left)
    {
        mbuf = buf;
        small = mmin (left, M25P128_PAGE_SIZE);

        if(small < M25P128_PAGE_SIZE)
        {
            for(i = 0; i < small; i++)
                tbuf[i] = buf[i];
            for(i = small; i < M25P128_PAGE_SIZE; i++)
                tbuf[i] = 0xff;
            mbuf = tbuf;
        }
        flash_ECCCalculate(mbuf, calcEcc);
        m25p128_write_noecc(pos, mbuf, M25P128_PAGE_SIZE);
        m25p128_write_noecc(ECC_BASE + pos / M25P128_PAGE_SIZE * 3, calcEcc, 3);
        left -= small;
        pos += small;
        buf += small;
    }
    return (n);
}

int
m25p128_write_noecc (int offset, void *buffer, size_t n)
{
    unsigned int pos = offset;
    unsigned int col = pos & (M25P128_PAGE_SIZE - 1);
    unsigned char *buf = (unsigned char *) buffer;
    int left = n;
    int i, j, ret, small;

    if(!SPI_FLASH_INITD)
        return 0;
    ret = 0;

    while (left)
    {
        small = mmin (left, (M25P128_PAGE_SIZE-col));

        set_cs (0);
        flash_writeb_cmd (WREN);
        set_cs (1);
        delay(1);
        set_cs (0);
        flash_writeb_cmd (PP);
        flash_writeb_cmd ((pos >> 16) & 0xff);
        flash_writeb_cmd ((pos >> 8) & 0xff);
        flash_writeb_cmd (pos & 0xff);
        for (i = 0; i < small; i++)
            flash_writeb_cmd (buf[i]);
        set_cs (1);
        delay(1);
        dotik (3000, 0);
        //wait for 20ms
        for (j = 0; j < 20; j++)
        {
            ret = m25p128_busy_wait_1ms ();
            if (!ret)
                break;
        }
        if (ret)
        {
            printf("write timeout\n");
            return (n - left);
        }
        m25p128_slow_read_noecc(pos, m25p128_buf, small);
        for(j = 0; j < small; j++)
        {
            if(buf[j] != m25p128_buf[j])
            {
                printf("write verify error at :%x, s:%x, t:%x\n", pos+j, buf[j], m25p128_buf[j]);
                return (n - left);
            }
        }
        left -= small;
        pos += small;
        buf += small;
        col = 0;
    }
    return (n);
}

int m25p128_write0 (int offset, void *buffer, size_t n)
{
    //m25p128_write_noecc(offset, buffer, n);
    m25p128_write_ecc(offset, buffer, n);
}

int
m25p128_sector_erase (int offset, size_t n)
{
    unsigned int pos = offset & (~(M25P128_PAGE_SIZE - 1));
    int left =
        ((offset & (M25P128_PAGE_SIZE - 1)) + n + M25P128_PAGE_SIZE - 1) / M25P128_PAGE_SIZE;
    int j, ret;
    unsigned int * buf;

    if(!SPI_FLASH_INITD)
        return 1;
    ret = 0;
    printf("erase addr: %x, %d pages\n", pos, left);

    while (left)
    {
        set_cs (0);
        flash_writeb_cmd (WREN);
        set_cs (1);
        delay(1);
        set_cs (0);
        flash_writeb_cmd (SE);
        flash_writeb_cmd ((pos >> 16) & 0xff);
        flash_writeb_cmd ((pos >> 8) & 0xff);
        flash_writeb_cmd (pos & 0xff);
        set_cs (1);
        delay(1);
        //wait for 20s
        for (j = 0; j < 20000; j++)
        {
            ret = m25p128_busy_wait_1ms ();
            if (!ret)
                break;
        }
        if (ret)
        {
            printf("erase timeout\n");
            return 1;
        }
        printf("erase sector 0x%x over, now verify\n", pos);
        m25p128_slow_read_noecc(pos, m25p128_buf, M25P128_PAGE_SIZE);
        buf = (unsigned int *)m25p128_buf;
        for(j = 0; j < M25P128_PAGE_SIZE/4; j++)
        {
            if(0xffffffff != buf[j])
            {
                printf("erase verify error at %x\n", pos+4*j);
                return 1;
            }
        }
        left--;
        pos += M25P128_PAGE_SIZE;
    }
    printf("verify over\n");
    return 0;
}

    int
m25p128_bulk_erase (void)
{
    int i, j, ret;
    unsigned int * buf;

    if(!SPI_FLASH_INITD)
        return 1;
    ret = 0;
    printf("bulk erase\n");
    set_cs (0);
    flash_writeb_cmd (WREN);
    set_cs (1);
    delay(1);
    set_cs (0);
    flash_writeb_cmd (BE);
    set_cs (1);
    delay(20);
    //wait for 400s
    for (j = 0; j < 400000; j++)
    {
        ret = m25p128_busy_wait_1ms ();
        dotik (2560, 0);
        if (!ret)
            break;
    }
    if(ret)
    {
        printf("erase timeout\n");
        return ret;
    }
    printf("erase over, now verify\n");
    for(i = 0; i < M25P128_SIZE/512; i++)
    {
        m25p128_slow_read_noecc(i*512, m25p128_buf, 512);
        buf = (unsigned int *)m25p128_buf;
        for(j = 0; j < 512/4; j++)
            if(buf[j] != 0xffffffff)
            {
                printf("erase verify err at %x\n", i*512+j*4);
                return 1;
            }
    }
    printf("verify over\n");
    return 0;
}


    void
m25p128_strategy (struct buf *bp)
{
    unsigned int dev, blkno, blkcnt;
    unsigned int d_secsize;
    int ret;
    char *buf;

    dev = bp->b_dev;
    blkno = bp->b_blkno;		//offset

    buf = bp->b_data;		//buf
    blkcnt = bp->b_bcount;	//len

    d_secsize = 512;


    //      blkcnt = howmany(bp->b_bcount, d_secsize); 


    /* Valid request?  */
    if (bp->b_blkno < 0 ||
            (bp->b_bcount % d_secsize) != 0 ||
            (bp->b_bcount / d_secsize) >= (1 << NBBY))
    {				//?
        bp->b_error = EINVAL;
        printf ("Invalid request \n");
        goto bad;
    }

    /* If it's a null transfer, return immediately. */
    if (bp->b_bcount == 0)
        goto done;


    if (bp->b_flags & B_READ)
    {
        if ((unsigned long) bp->b_data & (d_secsize - 1))
        {
            ret =
                m25p128_read0 (blkno * 512, (unsigned long *) m25p128_buf,
                        blkcnt);
            memcpy (bp->b_data, m25p128_buf, bp->b_bcount);
        }
        else
            ret =
                m25p128_read0 (blkno * 512, (unsigned long *) bp->b_data, blkcnt);
        if (ret != blkcnt)
            bp->b_flags |= B_ERROR;
    }

done:
    biodone (bp);
    return;
bad:
    bp->b_flags |= B_ERROR;
    biodone (bp);
}

    int
m25p128_open (dev_t dev, int flag, int fmt, struct proc *p)
{
    return 0;
}

    int
m25p128_close (dev_t dev, int flag, int fmt, struct proc *p)
{
    return 0;
}

    int
m25p128_read (dev_t dev, struct uio *uio, int ioflag)
{
    return physio (m25p128_strategy, NULL, dev, B_READ, NULL, uio);
}

    int
m25p128_write (dev_t dev, struct uio *uio, int ioflag)
{
    return -EINVAL;
}
