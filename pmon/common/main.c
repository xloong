/* $Id: main.c,v 1.1.1.1 2006/09/14 01:59:08 root Exp $ */

/*
 * Copyright (c) 2001-2002 Opsycon AB  (www.opsycon.se / www.opsycon.com)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by Opsycon AB, Sweden.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *  This code was created from code released to Public Domain
 *  by LSI Logic and Algorithmics UK.
 */ 

#include <stdio.h>
#include <string.h> 
#include <machine/pio.h>
#include <pmon.h>
#include <termio.h>
#include <endian.h>
#include <string.h>
#include <signal.h>
#include <setjmp.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#ifdef _KERNEL
#undef _KERNEL
#include <sys/ioctl.h>
#define _KERNEL
#else
#include <sys/ioctl.h>
#endif
#include <pmon.h>
#include <exec.h>
#include <file.h>

#include "mod_debugger.h"
#include "mod_symbols.h"

#include "sd.h"
#include "wd.h"


#include <pflash.h>
#include <flash.h>
#include <dev/pflash_tgt.h>
extern void    *callvec;
unsigned int show_menu;

#include "cmd_hist.h"		/* Test if command history selected */
#include "cmd_more.h"		/* Test if more command is selected */

unsigned char	last_two_8169_mac[2];
jmp_buf         jmpb;		/* non-local goto jump buffer */
char            line[LINESZ + 1];	/* input line */
struct termio	clntterm;	/* client terminal mode */
struct termio	consterm;	/* console terminal mode */
register_t	initial_sr;
int             memorysize;
int             memorysize_high;
char            prnbuf[LINESZ + 8];	/* commonly used print buffer */

int             repeating_cmd;
unsigned int  	moresz = 10;
#ifdef AUTOLOAD
static void autoload __P((char *));
#else
static void autorun __P((char *));
#endif
extern void __init __P((void));
extern void _exit (int retval);
extern void delay __P((int));

#ifdef INET
static void
pmon_intr (int dummy)
{
    sigsetmask (0);
    longjmp (jmpb, 1);
}
#endif

/*FILE *logfp; = stdout; */

#if NCMD_HIST == 0
void
get_line(char *line, int how)
{
	int i;

	i = read (STDIN, line, LINESZ);
	if(i > 0) {
		i--;
	}
	line[i] = '\0';
}
#endif


static int load_menu_list()
{
        char* rootdev = NULL;
        char* path = NULL;


                show_menu=1;
                if (path == NULL)
                {
                        path = malloc(512);
                        if (path == NULL)
                        {
                                return 0;
                        }
                }

                memset(path, 0, 512);
                rootdev = getenv("bootdev");
                if (rootdev == NULL)
                {
                        rootdev = "/dev/fs/ext2@wd0";
                }

                sprintf(path, "%s/boot/boot.cfg", rootdev);
                if (check_config(path) == 1)
                {
                        sprintf(path, "bl -d ide %s/boot/boot.cfg", rootdev);
                        if (do_cmd(path) == 0)
                        {
                                show_menu = 0;
                                //                                      video_cls();
                                free(path);
                                path = NULL;
                                return 1;
                        }
                }
                else
                {
                        sprintf(path, "/dev/fs/ext2@wd0/boot/boot.cfg", rootdev);
                        if (check_config(path) == 1)
                        {
                                sprintf(path, "bl -d ide /dev/fs/ext2@wd0/boot/boot.cfg", rootdev);
                                if (do_cmd(path) == 0)
                                {
                                        show_menu = 0;
                                        //                                              video_cls();
                                        free(path);
                                        path = NULL;
                                        return 1;
                                }
                        }
                }
#if 0
                if( check_ide() == 1 )// GOT IDE
                {
                        if( do_cmd ("bl -d ide /dev/fs/ext2@wd0/boot.cfg") ==0 )
                        {
                                show_menu=0;
                                video_cls();
                                return 1;
                        }
                }
                else if( check_cdrom () == 1 ) // GOT CDROM
                {
                        if( do_cmd ("bl -d cdrom /dev/fs/ext2@wd0/boot.cfg") ==0 )
                        {
                                show_menu=0;
                                video_cls();
                                return 1;
                        }
                }
#endif
                free(path);
                path = NULL;
                //                      video_cls();
                show_menu=0;
                return 0;
        show_menu=0;
        return 1;

}

int check_user_password()
{
	char buf[50];
	struct termio tty;
	int i;
	char c;
	if(!pwd_exist()||!pwd_is_set("user"))
		return 0;

	for(i=0;i<2;i++)
	{
	ioctl(i,TCGETA,&tty);
	tty.c_lflag &= ~ ECHO;
	ioctl(i,TCSETAW,&tty);
	}


	printf("\nPlease input user password:");
loop0:
	for(i= 0;i<50;i++)
	{
		c=getchar();
		if(c!='\n'&&c!='\r'){	
			printf("*");
			buf[i] = c;
		}
		else
		{
			buf[i]='\0';
			break;
		}
	}
	
	if(!pwd_cmp("user",buf))
	{
		printf("\nPassword error!\n");
		printf("Please input user password:");
		goto loop0;
	}

	for(i=0;i<2;i++)
	{
	tty.c_lflag |=  ECHO;
	ioctl(i,TCSETAW,&tty);
	}
			
	return 0;
}

int check_admin_password()
{
	char buf[50];
	struct termio tty;
	int i;
	char c;
	if(!pwd_exist()||!pwd_is_set("admin"))
		return 0;

	for(i=0;i<2;i++)
	{
	ioctl(i,TCGETA,&tty);
	tty.c_lflag &= ~ ECHO;
	ioctl(i,TCSETAW,&tty);
	}


	printf("\nPlease input admin password:");
loop1:
	for(i= 0;i<50;i++)
	{
		c=getchar();
		if(c!='\n'&&c!='\r'){	
			printf("*");
			buf[i] = c;
		}
		else
		{
			buf[i]='\0';
			break;
		}
	}
	
	if(!pwd_cmp("admin",buf))
	{
		printf("\nPassword error!\n");
		printf("Please input admin password:");
		goto loop1;
	}


	for(i=0;i<2;i++)
	{
	tty.c_lflag |=  ECHO;
	ioctl(i,TCSETAW,&tty);
	}
	
	return 0;
}


int check_sys_password()
{
	char buf[50];
	struct termio tty;
	int i;
	char c;
	int count=0;
	if(!pwd_exist()||!pwd_is_set("sys"))
		return 0;

	for(i=0;i<6;i++)
	{
	ioctl(i,TCGETA,&tty);
	tty.c_lflag &= ~ ECHO;
	ioctl(i,TCSETAW,&tty);
	}


	printf("\nPlease input sys password:");
loop1:
	for(i= 0;i<50;i++)
	{
		c=getchar();
		if(c!='\n'&&c!='\r'){	
			printf("*");
			buf[i] = c;
		}
		else
		{
			buf[i]='\0';
			break;
		}
	}
	
	if(!pwd_cmp("sys",buf))
	{
		printf("\nPassword error!\n");
		printf("Please input sys password:");
		count++;
		if(count==3)
			return -1;
		goto loop1;
	}


	for(i=0;i<6;i++)
	{
	tty.c_lflag |=  ECHO;
	ioctl(i,TCSETAW,&tty);
	}
	
	return 0;
}

/*
 *  Main interactive command loop
 *  -----------------------------
 *
 *  Clean up removing breakpoints etc and enter main command loop.
 *  Read commands from the keyboard and execute. When executing a
 *  command this is where <crtl-c> takes us back.
 */
void __gccmain(void);
void __gccmain(void)
{
}
int
main()
{
	char prompt[32];

	if (setjmp(jmpb)) {
		/* Bailing out, restore */
		closelst(0);
		ioctl(STDIN, TCSETAF, &consterm);
		printf(" break!\r\n");
	}

#ifdef INET
	signal (SIGINT, pmon_intr);
#else
	ioctl (STDIN, SETINTR, jmpb);
#endif

#if NMOD_DEBUGGER > 0
	rm_bpts();
#endif
        
	md_setsr(NULL, initial_sr);	/* XXX does this belong here? */

#if 0
	{

		check_user_password();
		if(!getenv("al"))
		load_menu_list();
	}
		load_menu_list();
#endif

#if 0 //INVERT the INT of cs5536
        do_cmd("cs5536_gpio 12");
        do_cmd("cs5536_gpio 12 out_inv:1");
#endif

{
static int run=0;
char *s;
char buf[LINESZ];
if(!run)
{
	run=1;
    save_ddrparam();
#ifdef AUTOLOAD
	s = getenv ("al");
	//autoload (s);
    autoload ("/dev/fs/ext2@wd0/boot/vmlinux.test2");
    //s = "ifaddr rtl0 10.2.0.129";
    //do_cmd (s);
    ////s = "load tftp://10.2.5.9/vmlinux.whd.debug.1";
    //while(1) {
    //strcpy(buf,"load tftp://10.2.5.9/vmlinux.whd.debug.1");
    //do_cmd(buf);}
#else
	s = getenv ("autoboot");
	autorun (s);
#endif
}
}

	while(1) {
#if 0
		while(1){char c;int i;
			i=term_read(0,&c,1);
			printf("haha:%d,%02x\n",i,c);
		}
#endif		
		//strncpy (prompt, getenv ("prompt"), sizeof(prompt));
		strncpy (prompt, "PMON>", sizeof("PMON>"));

#if NCMD_HIST > 0
		if (strchr(prompt, '!') != 0) {
			char tmp[8], *p;
			p = strchr(prompt, '!');
			strdchr(p);	/* delete the bang */
			sprintf(tmp, "%d", histno);
			stristr(p, tmp);
		}
#endif

		printf("%s", prompt);
#if NCMD_HIST > 0
		get_cmd(line);
#else
		get_line(line, 0);
#endif
		do_cmd(line);
		console_state(1);
	}
	return(0);
}

char bootp_c_ip_string[15];
char bootp_s_ip_string[15];
char bootp_file[129];
char bootp_opt[129];

char dhcp_c_ip_string[15];
char dhcp_s_ip_string[15];
char dhcp_sname[65];
char dhcp_file[129];
char dhcp_host_name[309];
char dhcp_root_path[309];
#ifdef AUTOLOAD
static void
autoload(char *s)
{
	char buf[LINESZ];
	char *pa;
	char *rd;
	unsigned int dly, lastt;
	unsigned int cnt;
	struct termio sav;
	unsigned int ip_last;
    char ip_string[15];

	if(s != NULL  && strlen(s) != 0) {
		dly = 3;

		printf("The last two byte of MAC address is %02x %02x\n", last_two_8169_mac[0], last_two_8169_mac[1]);
		ip_last = (unsigned int)(last_two_8169_mac[1]);
		if (ip_last > 254) {
			printf("ERROR -- MAC Address is not set correctly!!!\n");
			printf("Please configure this system manually.\n");
		}
		else {
/* 			printf("The IP will be configured as 192.168.11.%d\n", ip_last);
 * 			sprintf(ip_string, "192.168.11.%d", ip_last);
 */
			printf("The IP will be configured as 10.10.112.%d\n", ip_last);
			sprintf(ip_string, "10.10.112.201");
		}

		SBD_DISPLAY ("AUTO", CHKPNT_AUTO);
		printf("Press <Enter> to execute loading image:%s\n",s);
		printf("Press any other key to abort.\n");
		ioctl (STDIN, CBREAK, &sav);
		lastt = 0;
		do {
			delay(1000000);
			printf ("\b\b%02d", --dly);
			//printf (".", --dly);
			ioctl (STDIN, FIONREAD, &cnt);
		} while (dly != 0 && cnt == 0);

		if(cnt > 0 && strchr("\n\r", getchar())) {
			cnt = 0;
		}

		ioctl (STDIN, TCSETAF, &sav);
		putchar ('\n');

		if(cnt == 0) {
#define IDHCP 1
#define IBOOTP 0
#if IDHCP
			strcpy(buf, "ifaddr rtk0 lwdhcp");
			printf("========> %s\n",buf);
			do_cmd(buf);

            sprintf(buf, "load tftp://%s/%s", 
                    dhcp_s_ip_string, dhcp_file);
			//strcpy(buf,"load tftp://192.168.60.1/linux.27");
			printf("========> %s\n",buf);
			delay(1000000);
			do_cmd(buf);

			//sprintf(buf, "g console=ttyS0,115200 rw root=/dev/nfs nfsroot=192.168.60.1:/srv/nfsroot,rsize=65536,wsize=65536,intr,timeo=14 ip=%s:192.168.60.1::255.255.255.0:kd60-n%d:eth0:off", ip_string, ip_last);
//			sprintf(buf, "g ip=%s root=/dev/nfsroot nfsroot=192.168.11.222:/mnt/disk2 rw kid=%c", 
//                    bootp_c_ip_string, bootp_c_ip_string[strlen(bootp_c_ip_string)-1]);
//			sprintf(buf, "g ip=%s root=/dev/nfsroot nfsroot=192.168.11.1:/slave rw kid=%c", 
//                    bootp_c_ip_string, bootp_c_ip_string[strlen(bootp_c_ip_string)-1]);
			sprintf(buf, "g ip=%s root=/dev/nfsroot kid=%c %s", 
                    dhcp_c_ip_string, dhcp_c_ip_string[strlen(dhcp_c_ip_string)-1], dhcp_root_path);
			printf("========> %s\n",buf);
			delay(500000);
			do_cmd (buf);
#elif IBOOTP
			strcpy(buf, "ifaddr rtk0 bootp");
			printf("========> %s\n",buf);
			delay(500000);
			do_cmd(buf);

            sprintf(buf, "load tftp://%s/%s", 
                    bootp_s_ip_string, bootp_file);
			//strcpy(buf,"load tftp://192.168.60.1/linux.27");
			printf("========> %s\n",buf);
			delay(1000000);
			do_cmd(buf);

			//sprintf(buf, "g console=ttyS0,115200 rw root=/dev/nfs nfsroot=192.168.60.1:/srv/nfsroot,rsize=65536,wsize=65536,intr,timeo=14 ip=%s:192.168.60.1::255.255.255.0:kd60-n%d:eth0:off", ip_string, ip_last);
//			sprintf(buf, "g ip=%s root=/dev/nfsroot nfsroot=192.168.11.222:/mnt/disk2 rw kid=%c", 
//                    bootp_c_ip_string, bootp_c_ip_string[strlen(bootp_c_ip_string)-1]);
//			sprintf(buf, "g ip=%s root=/dev/nfsroot nfsroot=192.168.11.1:/slave rw kid=%c", 
//                    bootp_c_ip_string, bootp_c_ip_string[strlen(bootp_c_ip_string)-1]);
			sprintf(buf, "g ip=%s root=/dev/nfsroot nfsroot=%s:/mnt/disk2 rw kid=%c %s", 
                    bootp_c_ip_string, bootp_s_ip_string, bootp_c_ip_string[strlen(bootp_c_ip_string)-1], bootp_opt);
			printf("========> %s\n",buf);
			delay(500000);
			do_cmd (buf);
#else
#if 0
			strcpy(buf,"loadflash");
			//strcpy(buf,"load tftp://192.168.60.1/linux.27");
			printf("========> %s\n",buf);
			delay(500000);
			do_cmd(buf);
			sprintf(buf, "g ip=%s root=/dev/ram0 rw console=ttyS0,115200", ip_string);
			printf("========> %s\n",buf);
			delay(500000);
			do_cmd (buf);

            do_cmd("reboot");
#else
//			strcpy(buf,"set highmemsize 512");
//			printf("========> %s\n",buf);
//			delay(500000);
//			do_cmd(buf);

			strcpy(buf, "ifaddr rtk0 ");
			strcat(buf, ip_string);
			printf("========> %s\n",buf);
			delay(1000000);
			do_cmd(buf);

/* 			strcpy(buf,"load tftp://192.168.11.222/vmlinux_cf");
 */
			strcpy(buf,"load tftp://10.10.102.6/vmlinux-leo");
			printf("========> %s\n",buf);
			delay(1000000);
			do_cmd(buf);
//			sprintf(buf, "g ip=%s root=/dev/ram0 rw console=ttyS0,115200", ip_string);
/* 			sprintf(buf, "g console=ttyS0,115200 ip=%s root=/dev/nfs nfsroot=192.168.11.222:/mnt/disk2 rw ", ip_string); 
 */
			sprintf(buf, "g console=ttyS0,115200 ip=%s root=/dev/nfs nfsroot=10.10.102.6:/mnt/disk2 rw kid=2 masterip=10.10.112.101 ", ip_string); 
			printf("========> %s\n",buf);
			delay(500000);
			do_cmd (buf);

            do_cmd("reboot");
#endif 

#endif
		}
	}
}

#else
/*
 *  Handle autoboot execution
 *  -------------------------
 *
 *  Autoboot variable set. Countdown bootdelay to allow manual
 *  intervention. If CR is pressed skip counting. If var bootdelay
 *  is set use the value othervise default to 15 seconds.
 */
static void
autorun(char *s)
{
	char buf[LINESZ];
	char *d;
	unsigned int dly, lastt;
	unsigned int cnt;
	struct termio sav;

	if(s != NULL  && strlen(s) != 0) {
		d = getenv ("bootdelay");
		if(!d || !atob (&dly, d, 10) || dly < 0 || dly > 99) {
			dly = 15;
		}

		SBD_DISPLAY ("AUTO", CHKPNT_AUTO);
		printf("Autoboot command: \"%.60s\"\n", s);
		printf("Press <Enter> to execute or any other key to abort.\n");
		ioctl (STDIN, CBREAK, &sav);
		lastt = 0;
		dly++;
		do {
#if defined(HAVE_TOD) && defined(DELAY_INACURATE)
			time_t t;
			t = tgt_gettime ();
			if(t != lastt) {
				printf ("\r%2d", --dly);
				lastt = t;
			}
#else
			delay(1000000);
			printf ("\r%2d", --dly);
#endif
			ioctl (STDIN, FIONREAD, &cnt);
		} while (dly != 0 && cnt == 0);

		if(cnt > 0 && strchr("\n\r", getchar())) {
			cnt = 0;
		}

		ioctl (STDIN, TCSETAF, &sav);
		putchar ('\n');

		if(cnt == 0) {
			strcpy (buf, s);
			do_cmd (buf);
		}
	}
}
#endif
/*
 *  PMON2000 entrypoint. Called after initial setup.
 */
void
dbginit (char *adr)
{
	int	memsize, freq;
	char	fs[10], *fp;
	char	*s;

/*	splhigh();*/

	memsize = memorysize;

	__init();	/* Do all constructor initialisation */

	SBD_DISPLAY ("ENVI", CHKPNT_ENVI);
	envinit ();
#if defined(SMP)
	/* Turn on caches unless opted out */
	if (!getenv("nocache"))
		md_cacheon();
#endif

	SBD_DISPLAY ("SBDD", CHKPNT_SBDD);
	tgt_devinit();

#ifdef INET
	SBD_DISPLAY ("NETI", CHKPNT_NETI);
	init_net (1);
#endif

#if NCMD_HIST > 0
	SBD_DISPLAY ("HSTI", CHKPNT_HSTI);
	histinit ();
#endif

#if NMOD_SYMBOLS > 0
	SBD_DISPLAY ("SYMI", CHKPNT_SYMI);
	syminit ();
#endif

#ifdef DEMO
	SBD_DISPLAY ("DEMO", CHKPNT_DEMO);
	demoinit ();
#endif

	SBD_DISPLAY ("SBDE", CHKPNT_SBDE);
	initial_sr |= tgt_enable (tgt_getmachtype ());

#ifdef SR_FR
	Status = initial_sr & ~SR_FR; /* don't confuse naive clients */
#endif
	/* Set up initial console terminal state */
	ioctl(STDIN, TCGETA, &consterm);

#ifdef HAVE_LOGO
	tgt_logo();
#else
	tgt_printf ("\n * PMON2000 Professional *"); 
#endif
	tgt_printf(" after SMP2 \n");
	tgt_printf ("\nConfiguration [%s,%s", TARGETNAME,
			BYTE_ORDER == BIG_ENDIAN ? "EB" : "EL");
#ifdef INET
	tgt_printf (",NET");
#endif
#if NSD > 0
	printf (",SCSI");
#endif
#if NWD > 0
	printf (",IDE");
#endif
	printf ("]\nVersion: %s.\n", vers);
	printf ("Supported loaders [%s]\n", getExecString());
	printf ("Supported filesystems [%s]\n", getFSString());
	printf ("This software may be redistributed under the BSD copyright.\n");

	tgt_machprint();

	freq = tgt_pipefreq ();
	sprintf(fs, "%d", freq);
	fp = fs + strlen(fs) - 6;
	fp[3] = '\0';
	fp[2] = fp[1];
	fp[1] = fp[0];
	fp[0] = '.';
	printf (" %s MHz", fs);

	freq = tgt_cpufreq ();
	sprintf(fs, "%d", freq);
	fp = fs + strlen(fs) - 6;
	fp[3] = '\0';
	fp[2] = fp[1];
	fp[1] = fp[0];
	fp[0] = '.';
	printf (" / Bus @ %s MHz\n", fs);

	printf ("Memory size %3d MB (%3d MB Low memory, %3d MB High memory) .\n", (memsize+memorysize_high)>>20,
		(memsize>>20), (memorysize_high>>20));

	tgt_memprint();
#if defined(SMP)
	tgt_smpstartup();
#endif

	printf ("\n");

	md_clreg(NULL);
	md_setpc(NULL, (int32_t) CLIENTPC);
	md_setsp(NULL, tgt_clienttos ());
}

/*
 *  closelst(lst) -- Handle client state opens and terminal state.
 */
void
closelst(int lst)
{
	switch (lst) {
	case 0:
		/* XXX Close all LU's opened by client */
		break;

	case 1:
		break;

	case 2:
		/* reset client terminal state to consterm value */
		clntterm = consterm;
		break;
	}
}

/*
 *  console_state(lst) -- switches between PMON2000 and client tty setting.
 */
void
console_state(int lst)
{
	switch (lst) {
	case 1:
		/* save client terminal state and set PMON default */
		ioctl (STDIN, TCGETA, &clntterm);
		ioctl (STDIN, TCSETAW, &consterm);
		break;

	case 2:
		/* restore client terminal state */
		ioctl (STDIN, TCSETAF, &clntterm);
		break;
	}
}

/*************************************************************
 *  dotik(rate,use_ret)
 */
void
dotik (rate, use_ret)
	int             rate, use_ret;
{
static	int             tik_cnt;
static	const char      more_tiks[] = "|/-\\";
static	const char     *more_tik;

	tik_cnt -= rate;
	if (tik_cnt > 0) {
		return;
	}
	tik_cnt = 256000;
	if (more_tik == 0) {
		more_tik = more_tiks;
	}
	if (*more_tik == 0) {
		more_tik = more_tiks;
	}
	if (use_ret) {
		printf (" %c\r", *more_tik);
	} else {
		printf ("\b%c", *more_tik);
	}
	more_tik++;
}

#if NCMD_MORE == 0
/*
 *  Allow usage of more printout even if more is not compiled in.
 */
int
more (p, cnt, size)
     char           *p;
     int            *cnt, size;
{ 
	printf("%s\n", p);
	return(0);
}
#endif

/*
 *  Non direct command placeholder. Give info to user.
 */
int 
no_cmd(ac, av)
     int ac;
     char *av[];
{
    printf("Not a direct command! Use 'h %s' for more information.\n", av[0]);
    return (1);
}

/*
 *  Build argument area on 'clientstack' and set up clients
 *  argument registers in preparation for 'launch'.
 *  arg1 = argc, arg2 = argv, arg3 = envp, arg4 = callvector
 */

void
initstack (ac, av, addenv)
    int ac;
    char **av;
    int addenv;
{
	char	**vsp, *ssp;
	int	ec, stringlen, vectorlen, stacklen, i;
	register_t nsp;

	/*
	 *  Calculate the amount of stack space needed to build args.
	 */
	stringlen = 0;
	if (addenv) {
		envsize (&ec, &stringlen);
	}
	else {
		ec = 0;
	}
	for (i = 0; i < ac; i++) {
		stringlen += strlen(av[i]) + 1;
	}
	stringlen = (stringlen + 3) & ~3;	/* Round to words */
	vectorlen = (ac + ec + 2) * sizeof (char *);
	stacklen = ((vectorlen + stringlen) + 7) & ~7;

	/*
	 *  Allocate stack and us md code to set args.
	 */
	nsp = md_adjstack(NULL, 0) - stacklen;
	md_setargs(NULL, ac, nsp, nsp + (ac + 1) * sizeof(char *), (int)callvec);

	/* put $sp below vectors, leaving 32 byte argsave */
	md_adjstack(NULL, nsp - 32);
	memset((void *)((int)nsp - 32), 0, 32);

	/*
	 * Build argument vector and strings on stack.
	 * Vectors start at nsp; strings after vectors.
	 */
	vsp = (char **)(int)nsp;
	ssp = (char *)((int)nsp + vectorlen);

	for (i = 0; i < ac; i++) {
		*vsp++ = ssp;
		strcpy (ssp, av[i]);
		ssp += strlen(av[i]) + 1;
	}
	*vsp++ = (char *)0;

	/* build environment vector on stack */
	if (ec) {
		envbuild (vsp, ssp);
	}
	else {
		*vsp++ = (char *)0;
	}
	/*
	 * Finally set the link register to catch returning programs.
	 */
	md_setlr(NULL, (register_t)_exit);
}

#define u64 unsigned long long 
u64 __raw_readq(u64 q)
{
  u64 ret;

  asm volatile(
	  ".set mips3;\r\n"  \
	  "move	$8,%1;\r\n"	\
	  "ld	%0,0($8);\r\n"	\
	  :"=r"(ret)
	  :"r" (q)
	  :"$8");

	return ret;
}

u64 __raw_writeq(u64 addr, u64 val)
{
  
  u64 ret;

  asm volatile(
	  ".set mips3;\r\n"  \
	  "move	$8,%1;\r\n"	\
	  "move	$9,%2;\r\n"	\
	  "sd	$9,0($8);\r\n"	\
	  "ld	%0,0($8);\r\n"	\
	  :"=r"(ret)
	  :"r" (addr), "r" (val)
	  :"$8", "$9");

	return ret;
}
