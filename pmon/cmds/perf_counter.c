struct op_counter_config {
    unsigned long enabled;
    unsigned long event;
    unsigned long count;
    /*  Dummies because I am too lazy to hack the userspace tools.  */
    unsigned long kernel;
    unsigned long user;
    unsigned long exl;
    unsigned long unit_mask;
};

#define __read_64bit_c0_register(source, sel)               \
    ({ unsigned long long __res;                        \
        if (sel == 0)                      \
            __asm__ __volatile__(                   \
                            ".set\tmips3\n\t"               \
                            "dmfc0\t%0, " #source "\n\t"            \
                            ".set\tmips0"                   \
                            : "=r" (__res));                \
        else                                \
            __asm__ __volatile__(                   \
                            ".set\tmips64\n\t"              \
                            "dmfc0\t%0, " #source ", " #sel "\n\t"      \
                            ".set\tmips0"                   \
                            : "=r" (__res));                \
        __res;                              \
     })

#define __write_64bit_c0_register(register, sel, value)         \
    do {                                    \
            if (sel == 0)                      \
                __asm__ __volatile__(                   \
                                    ".set\tmips3\n\t"               \
                                    "dmtc0\t%z0, " #register "\n\t"         \
                                    ".set\tmips0"                   \
                                    : : "Jr" (value));              \
            else                                \
                __asm__ __volatile__(                   \
                                    ".set\tmips64\n\t"              \
                                    "dmtc0\t%z0, " #register ", " #sel "\n\t"   \
                                    ".set\tmips0"                   \
                                    : : "Jr" (value));              \
    } while (0)

#define LOONGSON_COUNTER1_EVENT(event)	((event&0x3f) << 5)
#define LOONGSON_COUNTER1_SUPERVISOR	(1UL    <<  2)
#define LOONGSON_COUNTER1_KERNEL		(1UL    <<  1)
#define LOONGSON_COUNTER1_USER		(1UL    <<  3)
#define LOONGSON_COUNTER1_ENABLE		(1UL    << 4)
#define LOONGSON_COUNTER1_OVERFLOW	(1ULL    << 63)
#define LOONGSON_COUNTER1_W		(1UL    << 30)
#define LOONGSON_COUNTER1_M		(1UL    << 31)
#define LOONGSON_COUNTER1_EXL			(1UL	<< 0)

#define LOONGSON_COUNTER2_EVENT(event)	((event&0x0f) << 5)
#define LOONGSON_COUNTER2_SUPERVISOR	LOONGSON_COUNTER1_SUPERVISOR
#define LOONGSON_COUNTER2_KERNEL		LOONGSON_COUNTER1_KERNEL
#define LOONGSON_COUNTER2_USER		LOONGSON_COUNTER1_USER
#define LOONGSON_COUNTER2_ENABLE		LOONGSON_COUNTER1_ENABLE
#define LOONGSON_COUNTER2_OVERFLOW	(1ULL   << 63)
#define LOONGSON_COUNTER2_W		(1UL    << 30)
#define LOONGSON_COUNTER2_M		(1UL    << 31)
#define LOONGSON_COUNTER2_EXL			(1UL	<< 0 )
#define LOONGSON_COUNTER_EXL		    (1UL << 0)	

/* Loongson2 PerfCount performance counter register */
#define read_c0_perflo1() __read_64bit_c0_register($25, 0)
#define write_c0_perflo1(val) __write_64bit_c0_register($25, 0, val)
#define read_c0_perfhi1() __read_64bit_c0_register($25, 1)
#define write_c0_perfhi1(val) __write_64bit_c0_register($25, 1, val)
#define read_c0_perflo2() __read_64bit_c0_register($25, 2)
#define write_c0_perflo2(val) __write_64bit_c0_register($25, 2, val)
#define read_c0_perfhi2() __read_64bit_c0_register($25, 3)
#define write_c0_perfhi2(val) __write_64bit_c0_register($25, 3, val)

static struct lg3_register_config {
	unsigned int control1;
	unsigned int control2;
	unsigned long long reset_counter1;
	unsigned long long reset_counter2;
	int ctr1_enable, ctr2_enable;
} reg;

static void lg3_reg_setup(struct op_counter_config *ctr)
{
	unsigned int control1 = 0;
	unsigned int control2 = 0;

	reg.reset_counter1 = 0;
	reg.reset_counter2 = 0;
	/* Compute the performance counter control word.  */
	/* For now count kernel and user mode */
	if (ctr[0].enabled){
		control1 |= LOONGSON_COUNTER1_EVENT(ctr[0].event) |
					LOONGSON_COUNTER1_ENABLE;
		if(ctr[0].kernel)
			control1 |= LOONGSON_COUNTER1_KERNEL;
		if(ctr[0].user)
			control1 |= LOONGSON_COUNTER1_USER;
//		reg.reset_counter1 = 0x8000000000000000ULL - ctr[0].count;
	}

	if (ctr[1].enabled){
		control2 |= LOONGSON_COUNTER2_EVENT(ctr[1].event) |
		           LOONGSON_COUNTER2_ENABLE;
		if(ctr[1].kernel)
			control2 |= LOONGSON_COUNTER2_KERNEL;
		if(ctr[1].user)
			control2 |= LOONGSON_COUNTER2_USER;
//		reg.reset_counter2 = (0x8000000000000000ULL- ctr[1].count) ;
	}

	if(ctr[0].enabled )
		control1 |= LOONGSON_COUNTER1_EXL;
	if(ctr[1].enabled )
		control2 |= LOONGSON_COUNTER2_EXL;

	reg.control1 = control1;
	reg.control2 = control2;

	reg.ctr1_enable = ctr[0].enabled;
	reg.ctr2_enable = ctr[1].enabled;
}

/* Program all of the registers in preparation for enabling profiling.  */

static void lg3_cpu_setup (void)
{
	unsigned long long perfcount1,perfcount2;

	perfcount1 = reg.reset_counter1; 
	perfcount2 = reg.reset_counter2; 
	write_c0_perfhi1(perfcount1);
	write_c0_perfhi2(perfcount2);
}

static void lg3_cpu_start(void)
{
	/* Start all counters on current CPU */
	reg.control1 |= (LOONGSON_COUNTER1_M |LOONGSON_COUNTER1_W);
	reg.control2 |= (LOONGSON_COUNTER2_M |LOONGSON_COUNTER1_W);
	/* Start all counters on current CPU */
	if(reg.ctr1_enable) {
		write_c0_perflo1(reg.control1);
	}
	if(reg.ctr2_enable) {
		write_c0_perflo2(reg.control2);
	}
}

static struct op_counter_config ctr[2];

void perf_ct_start()
{
    ctr[0].enabled = 1;
    ctr[1].enabled = 0;
    ctr[0].kernel = 1;
    ctr[0].user = 1
    ctr[1].kernel = 0;
    ctr[1].user = 0;
    ctr[0].event = ctr[1].event = 0;

    lg3_reg_setup();
    lg3_cpu_setup();
    lg3_cpu_start();
}

static void lg3_cpu_stop(void *args)
{
	write_c0_perflo1(0xc0000000);
	write_c0_perflo2(0x40000000);
	memset(&reg, 0, sizeof(reg));
}

void lg3_read_perf(void)
{
	counter1 = read_c0_perfhi1();
	counter2 = read_c0_perfhi2();
}

