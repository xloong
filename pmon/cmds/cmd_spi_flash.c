/*
 * =====================================================================================
 * cmd_spi_flash.c
 *
 * Description:
 *  
 *
 * Copyright (c) 2007 National Research Center for Intelligent Computing
 * Systems. ALL RIGHTS RESERVED. See "COPYRIGHT" for detail
 *
 * Created:  2010年08月08日 17时25分21秒
 * Revision:  none
 * Compiler:  gcc
 *
 * Author:  JiangTao (), jiangtao@ncic.ac.cn
 * Company:  CAR Group, NCIC,ICT,CAS.
 *
 * TODO:
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/endian.h>

#include <pmon.h>
extern void m25p128_readid(void);
extern void m25p128_init (int mode, int div);
extern int m25p128_read0 (int offset, void *buffer, size_t n);

int cmd_loadflash (int argc, char **argv)
{
	int size;
	char buf[LINESZ];

    m25p128_read0(0, &size, 4);
    size = (size + (1 << DEV_BSHIFT)-1) >> DEV_BSHIFT;    //size must be transfer to sector number
    sprintf(buf, "load /dev/disk/spi_flash0@1,%d", size);
    printf(" %s\n", buf);
	delay(500000);
    do_cmd(buf);

	return EXIT_SUCCESS;
}

int cmd_bootflash (int argc, char **argv)
{
	int i, size;
    char path[64];
	char * argvGo[16] = {
	   "boot",
	};
    m25p128_read0(0, &size, 4);
    size = (size + (1 << DEV_BSHIFT)-1) >> DEV_BSHIFT;    //size must be transfer to sector number
    sprintf(path, "/dev/disk/spi_flash0@1,%d", size);
    argvGo[1] = path;
	for(i=1;i<argc;i++)
		argvGo[i+1] = argv[i];
	cmd_boot(argc+1, argvGo);
	while(1);
	return EXIT_SUCCESS;
}
const Optdesc cmd_flash_write_opts[] =
{
	{"-s", "size of file"},
	{"-f", "flash_addr"},
	{"-o<offs>", "file addr"},
	{0}
};

extern int m25p128_write0 (int offset, void *buffer, size_t n);
int write_flash(int argc,char **argv)
{
    int c;
    int size, addr;
    int err = 0; 
    unsigned char * buf;
    if(argc != 7)
    {
        printf("arg err\n");
        return (1);
    }
    optind = 0;
	while ((c = getopt (argc, argv, "s:f:o:")) != EOF) {
        switch(c)
        {
            case 's':
                if (!get_rsa ((u_int32_t *)&size, optarg)) {
                    err++;
                }
                break;
            case 'f':
                if (!get_rsa ((u_int32_t *)&addr, optarg)) {
                    err++;
                }
                break;
            case 'o':
                if (!get_rsa ((u_int32_t *)&buf, optarg)) {
                    err++;
                }
                break;
            default:
                return 1;
                break;
        }
	}
	if(err)
		return (1);

    bulk_erase();
    m25p128_write0 (addr, buf, size);
    printf("write over\n");
    return 0;
}

const Optdesc cmd_flash_erase_opts[] =
{
	{"-s", "size of erase"},
	{"-f", "flash_addr"},
	{0}
};

extern int m25p128_sector_erase (int offset, size_t n);
int erase_flash(int argc,char **argv)
{
    int c;
    int size, addr;
    int err = 0; 
    if(argc != 5)
    {
        printf("arg err\n");
        return (1);
    }
    optind = 0;
	while ((c = getopt (argc, argv, "s:f:")) != EOF) {
        switch(c)
        {
            case 's':
                if (!get_rsa ((u_int32_t *)&size, optarg)) {
                    err++;
                }
                break;
            case 'f':
                if (!get_rsa ((u_int32_t *)&addr, optarg)) {
                    err++;
                }
                break;
            default:
                return 1;
                break;
        }
	}
	if(err)
		return (1);
    printf("sector erase at %x, size %x\n", addr, size);
    m25p128_sector_erase(addr, size);
    return 0;
}

extern int m25p128_bulk_erase(void);
int bulk_erase(int argc,char **argv)
{
    m25p128_bulk_erase();
    return 0;
}

int read_flash_id(int argc,char **argv)
{

    m25p128_readid();
    return 0;
}

const Optdesc cmd_flash_reset_opts[] =
{
	{"-m", "reset mode"},
	{"-d", "reset div"},
	{0}
};
int reset_flash(int argc,char **argv)
{
    int c;
    int err = 0; 
    int mode = 0;
    int div = 2;
    optind = 0;
    if(argc != 5)
    {
        printf("arg err\n");
        return (1);
    }
	while ((c = getopt (argc, argv, "m:d:")) != EOF) {
        switch(c)
        {
            case 'm':
                if (!get_rsa ((u_int32_t *)&mode, optarg)) {
                    err++;
                }
                break;
            case 'd':
                if (!get_rsa ((u_int32_t *)&div, optarg)) {
                    err++;
                }
                break;
            default:
                return 1;
                break;
        }
	}
	if(err)
		return (1);
    m25p128_init(mode, div);
    return 0;
}
static const Cmd Cmds[] =
{
	{"MyCmds"},
	{"loadflash","",0,"load kernel from flash",cmd_loadflash,0,99,CMD_REPEAT},
	{"bootflash","",0,"boot from flash",cmd_bootflash,0,99,CMD_REPEAT},
	{"flash_be","",0,"bulk erase flash",bulk_erase,0,99,CMD_REPEAT},
	{"flash_se","-s size -f flash_addr",cmd_flash_erase_opts,"sector erase flash", erase_flash, 0, 99, CMD_REPEAT},
	{"flash_w","-s size -f flash_addr -o file_addr",cmd_flash_write_opts,"write flash", write_flash, 0, 99, CMD_REPEAT},
	{"flash_id","",0,"read flash id",read_flash_id,0,99,CMD_REPEAT},
	{"reset_flash","-m mode -d div",cmd_flash_reset_opts,"reset flash",reset_flash,0,99,CMD_REPEAT},
	{0, 0}
};

static void init_cmd __P((void)) __attribute__ ((constructor));

static void
init_cmd()
{
	cmdlist_expand(Cmds, 1);
}

