/*	$Id: save_ddrparam.c,v 1.1.1.1 2006/09/14 01:59:08 root Exp $ */

/*
 * Copyright (c) 2001 Opsycon AB  (www.opsycon.se)
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by Opsycon AB, Sweden.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/device.h>
#include <sys/queue.h>

#include <pmon.h>

/*
 *  Save ddr controler register.
 */

#define u64 unsigned long long 

extern char ddr2_reg_data, ddr2_reg_data_mc1, _start;

extern u64 __raw_readq(u64 q);
extern u64 __raw_writeq(u64 addr, u64 val);

#define sd  __raw_writeq
#define ld  __raw_readq

//#define DEBUG

#define DDRCFG_STARTADDR  0x900000000ff00000ull

//static void disable_ddrconfig(int mc_selector)
static void disable_ddrconfig(void)
{
	unsigned long long val; 

	val = ld(0x900000001fe00180ull);
	val |= 0x100;
	/* Disable DDR access buffer */
	//val &=0xfffffffffffffdffull;
	//val &=0x7fffffeffull;
	sd(0x900000001fe00180ull, val);

#ifdef DEBUG
	printf("Now sys config reg:180 = %016llx\n", ld(0x900000001fe00180ull));
#endif
}

//static void enable_ddrconfig(int mc_selector)
static void enable_ddrconfig(void)
{
	unsigned long long val; 

    //printf("Test 113\n");

	val = ld(0x900000001fe00180ull);
	val &=0xfffffffffffffeffull;
	//val &=0x7fffffeffull;
	sd(0x900000001fe00180ull, val);

#ifdef DEBUG
	printf("Now sys config reg:180 = %016llx", ld(0x900000001fe00180ull));
#endif

}

#define DDR_PARAM_TOTAL	 180

void enable_ddrcfgwindow(int mc_selector)
{

#ifdef DEBUG

	printf("origin :: 0x00: %016llx  0x40: %016llx 0x80:  %016llx\n", ld(0x900000003ff00000ull),
			  ld(0x900000003ff00040ull),ld(0x900000003ff00080ull));
#endif


#if 0
	sd(0x900000003ff00000ull,0x0000000000000000ull);
	sd(0x900000003ff00040ull,0xfffffffff0000000ull);
	sd(0x900000003ff00080ull,0x00000000000000f0ull | mc_selector);
#else
	sd(0x900000003ff00000ull,0x000000000ff00000ull);
	sd(0x900000003ff00040ull,0xfffffffffff00000ull);
	sd(0x900000003ff00080ull,0x000000000ff000f0ull | mc_selector);
#endif

#ifdef DEBUG

	printf("0x00: %016llx  0x40: %016llx 0x80:  %016llx\n", ld(0x900000003ff00000ull),
			  ld(0x900000003ff00040ull),ld(0x900000003ff00080ull));
#endif

}

void disable_ddrcfgwidow(int mc_selector)
{
	sd(0x900000003ff00000ull,0x000000001fc00000ull);
	sd(0x900000003ff00040ull,0xfffffffffff00000ull);
	sd(0x900000003ff00080ull,0x000000001fc000f2ull);

#ifdef DEBUG
	printf("0x00: %016llx  0x40: %016llx 0x80:  %016llx\n", ld(0x900000003ff00000ull),
			  ld(0x900000003ff00040ull),ld(0x900000003ff00080ull));
#endif

}

#define MC0 0x0
#define MC1 0x1

int read_ddr_param(int mc_selector,  char * base)
{
	int i;
	unsigned long long * val = (unsigned long long *)base;

    //printf("Test 11\n");
	// step 1. Enabel access to MC0 or MC1 register space 
	enable_ddrconfig();

    //printf("Test 12\n");
	// step 2. Change The Primest window for MC0 or MC1 register space 
	enable_ddrcfgwindow(mc_selector);

	// step 3. Read out ddr config register to buffer

	// Notice here: It maybe fail because memory access is disabled at this moment, 
	// if this comes true, you can try write it into flash chip directly.
	printf("Now Read out DDR parameter from DDR MC0/MC1 controler after DDR training  \n");
	for ( i = DDR_PARAM_TOTAL - 1; i >= 0; i--) // NOTICE HERE: it means system has DDR_PARAM_TOTAL double words
	{
		//addrs[i] = DDRCFG_STARTADDR   + 0x800 * i; 
		val[i] =  ld(DDRCFG_STARTADDR + 0x10 * i); 
		//val[i] =  (DDRCFG_STARTADDR + 0x8 * i); 

#ifdef DEBUG
		printf("< CFGREG >:val[%d]  = %016llx \n", i, val[i]); 
#endif
	}
    //clear param_start
    val[3]  &=  0xfffffeffffffffff;

	// step 4. Disabel access to MC0 or MC1 register space 
	disable_ddrconfig();

	// step 5. Restore The Primest window for accessing system memory
	disable_ddrcfgwidow(mc_selector);

	printf("Read out DDR MC%d config Done.\n", mc_selector);
	return 0;
}

#define FLASH_CAPACITY  0x80000 //1M byte
#define REPLACE_DDRPARAM

void save_ddrparam(void)
{

  int array[]={0x1234,0x5678,0x9abc,0xdef0};
  unsigned long long ddr_param_buf[DDR_PARAM_TOTAL + 1];
  unsigned long long tmp, tmp1, tmp2;
  int   i;

#ifdef TEST
  printf("%s:%d:%s \n", __FILE__,__LINE__,__FUNCTION__);
  tgt_flashprogram(0xbfc00000+((int)&ddr2_reg_data -(int)&_start),172*8,&ddr2_reg_data,TRUE);
  printf("\n%s:%d:%s \n", __FILE__,__LINE__,__FUNCTION__);
  tgt_flashprogram(0xbfc00000+0x80000,8,&array,TRUE);
  printf("\n%s:%d:%s \n", __FILE__,__LINE__,__FUNCTION__);

#else

  /********************************************************/
  /************************/ // End of flash 
  /*	  DDRPTOVF	      */ // End - 8 (byte) (1M-8)
  /* -------------------- */ //
  /*					  */ // 
  /*      .......	      */
  /*      .......	      */
  /*      .......	      */ // $ddr3_data
  /*      .......	      */
  /*					  */ 
  /************************/ // Base of flash: offset 0x00
  /********************************************************/

  //printf("Test 111\n");
  // step 1. Read out DDR controler register values and save them in buffers

    tmp1 = ld(0x900000003ff00090ull);
    tmp2 = ld(0x900000003ff00098ull);
  //printf("tmp1=0x%016llx\n", tmp1);
  //printf("tmp2=0x%016llx\n", tmp2);
    tmp1 &= 0xff;
    tmp2 &= 0xff;
#ifdef DEBUG
  printf("tmp1=0x%016llx\n", tmp1);
  printf("tmp2=0x%016llx\n", tmp2);
#endif
    if ((tmp1 == 0xf0) || (tmp2 == 0xf0))
    {
        tmp =  ld(0x900000001fc00000 + (int) &ddr2_reg_data - (int)&_start + 180 * 8);
        if (!tmp)
        {
            // step 1.1 Read out DDR controler register from MC0 and save them in buffer0
            read_ddr_param(MC0, ddr_param_buf);

            //set leveled mark
            ddr_param_buf[DDR_PARAM_TOTAL] = 0x0123456789abcdef;
            // step 1.2 Program buffers of MC0 register into FLASH 
#ifdef REPLACE_DDRPARAM
            tgt_flashprogram(0xbfc00000+((int)&ddr2_reg_data -(int)&_start),181*8,ddr_param_buf,TRUE);
#else // test mode
            tgt_flashprogram(0xbfc00000+((int)&ddr2_reg_data -(int)&_start),181*8,&ddr2_reg_data,TRUE);
#endif

#ifdef DEBUG
  for(i = 0; i< 180; i++)
    {
        tmp =  ld(0x900000001fc00000 + (int) &ddr2_reg_data - (int)&_start + i * 8);
        if(ddr_param_buf[i] != tmp)
        {
            printf("\nMiscompare:i=%d, val=%016llx", i, tmp);
        }
    }
#endif
        }
    }

    if ((tmp1 == 0xf1) || (tmp2 == 0xf1))
    {
        tmp =  ld(0x900000001fc00000 + (int) &ddr2_reg_data - (int)&_start + 180 * 8);
        if (!tmp)
        {
            // step 2.2 Read out DDR controler register from MC1 and save them in buffer1
            read_ddr_param(MC1, ddr_param_buf);

            //set leveled mark
            ddr_param_buf[DDR_PARAM_TOTAL] = 0x0123456789abcdef;
            // step 2.2 Program buffers of MC1 register into FLASH 
#ifdef REPLACE_DDRPARAM
            tgt_flashprogram(0xbfc00000+((int)&ddr2_reg_data -(int)&_start),181*8,ddr_param_buf,TRUE);
#else // test mode
            tgt_flashprogram(0xbfc00000+((int)&ddr2_reg_data -(int)&_start),181*8,&ddr2_reg_data,TRUE);
#endif

#ifdef DEBUG
  for(i = 0; i< 180; i++)
    {
        tmp =  ld(0x900000001fc00000 + (int) &ddr2_reg_data_mc1 - (int)&_start + i * 8);
        if(ddr_param_buf[i] != tmp)
        {
            printf("\nMiscompare:i=%d, val=%016llx", i, tmp);
        }
    }
#endif
        }
    }

  // step 3. Setup DDR Param Traning Over Flag
  //tgt_flashprogram(0xbfc00000+FLASH_CAPACITY-8, 8, &array, TRUE);

#endif

}

// test : master
int
cmd_save_ddrparam(ac, av)
	int ac;
	char *av[];
{
	printf("start save_ddrparam\n");
	save_ddrparam();
	printf("save_ddrparam done\n");
	return(1);
}


/*
 *
 *  Command table registration
 *  ==========================
 */

static const Cmd Cmds[] =
{
	{"Misc"},
	{"save_ddrparam",	"", 0, "Save ddr controler register into FALSH", cmd_save_ddrparam, 1, 99, 0},
	{0, 0}
};

static void init_cmd __P((void)) __attribute__ ((constructor));

void
init_cmd()
{
	cmdlist_expand(Cmds, 1);
}
