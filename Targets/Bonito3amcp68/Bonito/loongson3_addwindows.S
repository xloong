/*whd : loongson3_ddr2_config.S
        used to set up all ddr controllers
        and set up the memory space on L2 Xbar
*/

/* below options means only one memory controler is used */
//#define MC0_ONLY		  
//#define MC1_ONLY
#define MC1X2_ONLY

/* below options indicated both memory controler is used defaulty */

//#define INTERLEAVE_27
//#define INTERLEAVE_13
//#define INTERLEAVE_12
#define INTERLEAVE_11
//#define INTERLEAVE_10
//#define NO_INTERLEAVE

/* Size of each DDR controller */
//#define DDR_256
//#define DDR_512
//#define DDR_1G
#define DDR_2G

/* Micro for printf address X */
/* This micro will use: a0/a1/a2/a3 v0/s0 */
#define PPADDR(x)   \
        li	a0, ((x)>>32); \
        bal	hexserial; nop; \
        li	a0, ((x)); \
        bal	hexserial; nop; \
        TTYDBG("-"); \
        dli	a0, (x); lw     a0, 0x4(a0) ; bal	hexserial; nop; \
        dli	a0, (x); lw     a0, 0x0(a0) ; bal	hexserial; nop; \
        TTYDBG(" "); 

/* Micro for printf address X: 0x0, 0x40, 0x80. used for printf */
/* This micro will use: a0/a1/a2/a3 v0/s0, because it only call PPADDR */
#define PPWIN(x)   \
	PPADDR(x+0x0)  ; PPADDR(x+0x40) ; PPADDR(x+0x80); TTYDBG("\r\n"); \
	PPADDR(x+0x8)  ; PPADDR(x+0x48) ; PPADDR(x+0x88); TTYDBG("\r\n"); \
	PPADDR(x+0x10) ; PPADDR(x+0x50) ; PPADDR(x+0x90); TTYDBG("\r\n"); \
	PPADDR(x+0x18) ; PPADDR(x+0x58) ; PPADDR(x+0x98); TTYDBG("\r\n"); \
	PPADDR(x+0x20) ; PPADDR(x+0x60) ; PPADDR(x+0xa0); TTYDBG("\r\n"); \
	PPADDR(x+0x28) ; PPADDR(x+0x68) ; PPADDR(x+0xa8); TTYDBG("\r\n"); \
	PPADDR(x+0x30) ; PPADDR(x+0x70) ; PPADDR(x+0xb0); TTYDBG("\r\n"); \
	PPADDR(x+0x38) ; PPADDR(x+0x78) ; PPADDR(x+0xb8); TTYDBG("\r\n"); 

/* This micro will damage: a0/t0, because it only call PPADDR */
#define setaddrval(addr,val) \
	dli	t0, (val); dli	a0, (addr); sd	t0, 0(a0); sync;

/* used to set window BASE/MMASK/BASE register based on address 'baseaddr' */

/* This micro will damage: a0/t0/t1/t2 
// NOTICE: this setwin is just changed for ddr2_config
#define setwin(baseaddr, baseval, maskval, mmapval) \
	dli t1, (baseval); or t0,a0; dli t1,(maskval); dli t2, (mmapval); \
	//dli a0, (baseaddr);  \
	move s3, (baseaddr);  \
	sd  $0, 0x80(s3) ;sd  t0, 0x0(s3); sd  t1, 0x40(s3); sd  t2, 0x80(s3);
*/

//t0: real baseaddress 
//a0: cpu id
/* This micro will damage: s3/t1 
#define setwin(baseoffset, baseval, maskval, mmapval) \
	dli	s3,baseoffset; \
	daddu s3,t0;\
	or s3,a0; \
	dli t1, (baseval); \
	or t1,a0; \
	sd  t1, 0x0(s3);\
	dli t1,(maskval);\
	sd  t1, 0x40(s3);\
	sd  $0, 0x80(s3) ;\
	dli t1, (mmapval); \
	sd  t1, 0x80(s3);
*/

#define setwin(baseoffset, baseval, maskval, mmapval) \
	dli	s3,baseoffset; \
	daddu s3,t0;\
	or s3,a0; \
	dli t1, (baseval); \
	sd  t1, 0x0(s3);\
	dli t1,(maskval);\
	sd  t1, 0x40(s3);\
	sd  $0, 0x80(s3) ;\
	dli t1, (mmapval); \
	sd  t1, 0x80(s3);



/* This micro will damage: a0/t0/t1/t2 
#define setwin(baseaddr, baseval, maskval, mmapval) \
	//dli a0, (baseaddr);  \
	move a0, (baseaddr);  \
	dli t0, (baseval); dli t1,(maskval);   dli t2, (mmapval); \
	sd  $0, 0x80(a0) ;sd  t0, 0x0(a0); sd  t1, 0x40(a0); sd  t2, 0x80(a0);
*/
/****************** s1: [1:0]: NODE ID***************/
#define GET_NODE_ID_a0 dli a0, 0x3; and a0, s1, a0; dsll a0, 44;
# now assume cpu0: 0xc0000000 ~ 0xffffffff
#			 cpu1: 0x80000000 ~ 0xbfffffff

# 0x1_00000000 ~ 0x1_7fffffff : used to access other cpu core memory
# 0x0_e0000000 ~ 0x0_ffffffff : used to access local cpu perial

//#1. set CPU0 cpu windows
	    PRINTSTR("set CPU LX1 BAR  windows\r\n")
		li	s1, 0x0	
        GET_NODE_ID_a0;
        dli     t0, 0x900000003ff02000
        or      t0, t0, a0
		setwin(0x08,0x00000000e0000000 ,0xffffffffe0000000 , 0x00000000e00000f7)
		setwin(0x00,0x0000000100000000 ,0xffffffff00000000 , 0x00000001000000f6)

		GET_NODE_ID_a0;
        dli     t0, 0x900000003ff02100
        or      t0, t0, a0
		setwin(0x00,0x0000000100000000 ,0xffffffff80000000 , 0x00000001000000f6)
		setwin(0x08,0x00000000e0000000 ,0xffffffffe0000000 , 0x00000000e00000f7)

		GET_NODE_ID_a0;
        dli     t0, 0x900000003ff02200
        or      t0, t0, a0
		setwin(0x00,0x0000000100000000 ,0xffffffff80000000 , 0x00000001000000f6)
		setwin(0x08,0x00000000e0000000 ,0xffffffffe0000000 , 0x00000000e00000f7)

		GET_NODE_ID_a0;
        dli     t0, 0x900000003ff02300
        or      t0, t0, a0
		setwin(0x00,0x0000000100000000 ,0xffffffff80000000 , 0x00000001000000f6)
		setwin(0x08,0x00000000e0000000 ,0xffffffffe0000000 , 0x00000000e00000f7)


/*
		// below used to test 98002000... address
		li	s1, 0x0	
        GET_NODE_ID_a0;
        dli     t0, 0x900000003ff02000
        or      t0, t0, a0
		setwin(0x00,0x0000800000000000 ,0xfffff00000000c00 , 0x00008000000000f0)
		setwin(0x08,0x0000800000000400 ,0xfffff00000000c00 , 0x00008000000004f1)
		setwin(0x10,0x0000800000000800 ,0xfffff00000000c00 , 0x00008000000008f2)
		setwin(0x18,0x0000800000000c00 ,0xfffff00000000c00 , 0x0000800000000cf3)

		setwin(0x20,0x0000000000000000 ,0xffffffff00000c00 , 0x00000000000000f0)
		setwin(0x28,0x0000000000000400 ,0xffffffff00000c00 , 0x00000000000004f1)
		setwin(0x30,0x0000000000000800 ,0xffffffff00000c00 , 0x00000000000008f2)
		setwin(0x38,0x0000000000000c00 ,0xffffffff00000c00 , 0x0000000000000cf3)


		li	s1, 0x0	
        GET_NODE_ID_a0;
        dli     t0, 0x900000003ff02100
        or      t0, t0, a0

		setwin(0x00,0x0000800000000000 ,0xfffff00000000c00 , 0x00008000000000f0)
		setwin(0x08,0x0000800000000400 ,0xfffff00000000c00 , 0x00008000000004f1)
		setwin(0x10,0x0000800000000800 ,0xfffff00000000c00 , 0x00008000000008f2)
		setwin(0x18,0x0000800000000c00 ,0xfffff00000000c00 , 0x0000800000000cf3)

		setwin(0x20,0x0000000000000000 ,0xffffffff00000c00 , 0x00000000000000f0)
		setwin(0x28,0x0000000000000400 ,0xffffffff00000c00 , 0x00000000000004f1)
		setwin(0x30,0x0000000000000800 ,0xffffffff00000c00 , 0x00000000000008f2)
		setwin(0x38,0x0000000000000c00 ,0xffffffff00000c00 , 0x0000000000000cf3)


		li	s1, 0x0	
        GET_NODE_ID_a0;
        dli     t0, 0x900000003ff02200
        or      t0, t0, a0
		setwin(0x00,0x0000800000000000 ,0xfffff00000000c00 , 0x00008000000000f0)
		setwin(0x08,0x0000800000000400 ,0xfffff00000000c00 , 0x00008000000004f1)
		setwin(0x10,0x0000800000000800 ,0xfffff00000000c00 , 0x00008000000008f2)
		setwin(0x18,0x0000800000000c00 ,0xfffff00000000c00 , 0x0000800000000cf3)

		setwin(0x20,0x0000000000000000 ,0xffffffff00000c00 , 0x00000000000000f0)
		setwin(0x28,0x0000000000000400 ,0xffffffff00000c00 , 0x00000000000004f1)
		setwin(0x30,0x0000000000000800 ,0xffffffff00000c00 , 0x00000000000008f2)
		setwin(0x38,0x0000000000000c00 ,0xffffffff00000c00 , 0x0000000000000cf3)

		li	s1, 0x0	
        GET_NODE_ID_a0;
        dli     t0, 0x900000003ff02300
        or      t0, t0, a0
		setwin(0x00,0x0000800000000000 ,0xfffff00000000c00 , 0x00008000000000f0)
		setwin(0x08,0x0000800000000400 ,0xfffff00000000c00 , 0x00008000000004f1)
		setwin(0x10,0x0000800000000800 ,0xfffff00000000c00 , 0x00008000000008f2)
		setwin(0x18,0x0000800000000c00 ,0xfffff00000000c00 , 0x0000800000000cf3)

		setwin(0x20,0x0000000000000000 ,0xffffffff00000c00 , 0x00000000000000f0)
		setwin(0x28,0x0000000000000400 ,0xffffffff00000c00 , 0x00000000000004f1)
		setwin(0x30,0x0000000000000800 ,0xffffffff00000c00 , 0x00000000000008f2)
		setwin(0x38,0x0000000000000c00 ,0xffffffff00000c00 , 0x0000000000000cf3)

*/

//# 2. set CPU0 HT0  windows

	    PRINTSTR("set CPU HT0 windows\r\n")
		li	s1, 0x0	
        GET_NODE_ID_a0;
        dli     t0, 0x900000003ff02600
        or      t0, t0, a0
		setwin(0x00,0x0000000100000000 ,0xffffffff00000c00 , 0x00000001000000f0)
		setwin(0x08,0x0000000100000400 ,0xffffffff00000c00 , 0x00000001000004f1)
		setwin(0x10,0x0000000100000800 ,0xffffffff00000c00 , 0x00000001000008f2)
		setwin(0x18,0x0000000100000c00 ,0xffffffff00000c00 , 0x0000000100000cf3)


//# 3. open receive windows of CPU0 
#if 1//OPEN RX SPACE in HOST
	TTYDBG("HT0_LOW RX DMA address windows 0 ENABLED\r\n")
	dli	    t2, 0x90000cfdfb000060
	li	    t0, 0x80000000
	sw	    t0, 0x0(t2)
	li	    t0, 0x0000f000
	sw	    t0, 0x4(t2)

	TTYDBG("HT0_HIGH RX DMA address windows 0 ENABLED\r\n")
	dli	    t2, 0x90000dfdfb000060
	li	    t0, 0x80000000
	sw	    t0, 0x0(t2)
	li	    t0, 0x0000f000
	sw	    t0, 0x4(t2)

#endif


