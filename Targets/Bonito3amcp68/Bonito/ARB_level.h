/*****************************
    Macro defination for Access Result Based Software Leveling
    Author: Chen Xinke
    v0.1    for LS3A3
*******************************/
//select level which MC--2: MC1; 1: MC0
#define MC_SELECT 0x2
#define DDR_MC_CONFIG_BASE      0x900000000ff00000
#define START_ADDR              (0x30)
#define INT_STATUS_ADDR         (0x960)
#define RESYNC_DLL_ADDR         (0x980)
#define START_OFFSET            40
#define INIT_COMPLETE_OFFSET    8
#define RESYNC_DLL_OFFSET       16

#define CLKLVL_DELAY_2_ADDR      (0x8f0)
#define CLKLVL_DELAY_1_ADDR      (0x8f0)
#define CLKLVL_DELAY_0_ADDR      (0x8f0)
#define CLKLVL_DELAY_2_OFFSET    24
#define CLKLVL_DELAY_1_OFFSET    16
#define CLKLVL_DELAY_0_OFFSET    8

#define WRLVL_DELAY_8_ADDR      (0xb10)
#define WRLVL_DELAY_7_ADDR      (0xb10)
#define WRLVL_DELAY_6_ADDR      (0xb10)
#define WRLVL_DELAY_5_ADDR      (0xb00)
#define WRLVL_DELAY_4_ADDR      (0xb00)
#define WRLVL_DELAY_3_ADDR      (0xb00)
#define WRLVL_DELAY_2_ADDR      (0xb00)
#define WRLVL_DELAY_1_ADDR      (0xaf0)
#define WRLVL_DELAY_0_ADDR      (0xaf0)
#define WRLVL_DELAY_8_OFFSET    32
#define WRLVL_DELAY_7_OFFSET    16
#define WRLVL_DELAY_6_OFFSET    0
#define WRLVL_DELAY_5_OFFSET    48
#define WRLVL_DELAY_4_OFFSET    32
#define WRLVL_DELAY_3_OFFSET    16
#define WRLVL_DELAY_2_OFFSET    0
#define WRLVL_DELAY_1_OFFSET    48
#define WRLVL_DELAY_0_OFFSET    32

#define RDLVL_GATE_DELAY_8_ADDR (0xa90)
#define RDLVL_GATE_DELAY_7_ADDR (0xa90)
#define RDLVL_GATE_DELAY_6_ADDR (0xa90)
#define RDLVL_GATE_DELAY_5_ADDR (0xa90)
#define RDLVL_GATE_DELAY_4_ADDR (0xa80)
#define RDLVL_GATE_DELAY_3_ADDR (0xa80)
#define RDLVL_GATE_DELAY_2_ADDR (0xa80)
#define RDLVL_GATE_DELAY_1_ADDR (0xa80)
#define RDLVL_GATE_DELAY_0_ADDR (0xa70)
#define RDLVL_GATE_DELAY_8_OFFSET    48
#define RDLVL_GATE_DELAY_7_OFFSET    32
#define RDLVL_GATE_DELAY_6_OFFSET    16
#define RDLVL_GATE_DELAY_5_OFFSET    0
#define RDLVL_GATE_DELAY_4_OFFSET    48
#define RDLVL_GATE_DELAY_3_OFFSET    32
#define RDLVL_GATE_DELAY_2_OFFSET    16
#define RDLVL_GATE_DELAY_1_OFFSET    0
#define RDLVL_GATE_DELAY_0_OFFSET    48

#define RDLVL_DELAY_8_ADDR      (0xa50)
#define RDLVL_DELAY_7_ADDR      (0xa50)
#define RDLVL_DELAY_6_ADDR      (0xa40)
#define RDLVL_DELAY_5_ADDR      (0xa40)
#define RDLVL_DELAY_4_ADDR      (0xa40)
#define RDLVL_DELAY_3_ADDR      (0xa40)
#define RDLVL_DELAY_2_ADDR      (0xa30)
#define RDLVL_DELAY_1_ADDR      (0xa30)
#define RDLVL_DELAY_0_ADDR      (0xa30)
#define RDLVL_DELAY_8_OFFSET    16
#define RDLVL_DELAY_7_OFFSET    0
#define RDLVL_DELAY_6_OFFSET    48
#define RDLVL_DELAY_5_OFFSET    32
#define RDLVL_DELAY_4_OFFSET    16
#define RDLVL_DELAY_3_OFFSET    0
#define RDLVL_DELAY_2_OFFSET    48
#define RDLVL_DELAY_1_OFFSET    32
#define RDLVL_DELAY_0_OFFSET    16

#define LVL_FIRST_STEP     (0x8)
#define LVL_SECOND_STEP    (0x1)
#define CLKLVL_MAX_DELAY        (0x80)
#define WRLVL_MAX_DELAY         (0x80)
#define RDLVL_MAX_DELAY         (0x80)
#define RDLVL_GATE_MAX_DELAY    (0x22)

#define ARB_LEVEL_NODE_ID   0x0
#define SET_CURRESP  dli  a1, 0x01; or   t3, t3, a1;
#define CLR_CURRESP  dli  a1, 0xfffffffffffffffe; and    t3, t3, a1;
#define GET_CURRESP  dli  a1, 0x01; and  a1, t3, a1;

#define SET_LVLSTAT  dli a1, 0x01;  or  t4, t4, a1;
#define CLR_LVLSTAT  dli a1, 0xfffffffffffffffe; and     t4, t4, a1;
#define GET_LVLSTAT  dli a1, 0x01;  and  a1, t4, a1;

#define SET_FIRST_LVL   dli a1, 0x8000000000000000; or  t4, t4, a1;
#define CLR_FIRST_LVL   dli a1, 0x7fffffffffffffff; and t4, t4, a1;
#define GET_FIRST_LVL   dsrl a1, t4, 63;

#define ARB_STORE_BASE  0x9800000000000600  //(600 ~ 800 -- 512B max, 64 registers)
#define ARB_STACK_BASE  0x9800000000000800  //(800 ~ a00 -- 512B max, 64 registers)
#define B0_TM_RST   (0x0)
#define B1_TM_RST   (0x8)
#define B2_TM_RST   (0x10)
#define B3_TM_RST   (0x18)
#define B4_TM_RST   (0x20)
#define B5_TM_RST   (0x28)
#define B6_TM_RST   (0x30)
#define B7_TM_RST   (0x38)
//for all 8 bytes
#define GD_MAX      (0x40)
#define GD_MIN      (0x48)
#define GD_MID      (0x50)
#define LEVEL_SEQ_BASE   (0x60)

