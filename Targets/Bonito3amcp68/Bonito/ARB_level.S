/************************************
    Function:   Access Result Based Software leveling
    Author: Chen Xinke
    v1.x    Single Node mode
    v1.1    Raw code for LS3A3
    v0.2    used for test
    
note: don't use s0, because it will be use at some subroutine
use register: a0,a1,a2,v0,v1, t9

register usage:
s3: Level sequence pointer.
t0: RST store bit pointer during TM process;
    RST parse pointer during result process.
t1: 
t2: currently used delay value.
t3, t4: variables
t5: delay value interval.
t6: param to Modify_param.
t7: volatile value
t8: ARB_STORE_BASE
t9: one level result: 0: success; 1: Fail.
v0, v1: return value of test_mem.
algrithm: 

***************************************/
/********************************
********************************/
#include "ARB_level.h"

//#define DEBUG_ARB_LEVEL
#define WINDOW_ZERO_NUM     0xc
#define LOG2_STEP   1   //log2 of TM step interval

ARB_level:
    move    t9, ra
ARB_start:
/*
 *Lock Scache 9800?00000000000 ~ 9800?00000001000(4K)
 */
    PRINTSTR("\r\nLock Scache Node x--9800?00000000000~4K...\r\n")
    dli     a2, 0x900000003ff00200
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   a2, a2, a1
    dli     a3, 0x8000000000000000
    daddu   a3, a3, a1
    sd      a3, 0x0(a2)
    dli     a3, 0x0000fffffffff000
    sd      a3, 0x40(a2)
    PRINTSTR("Lock Scache Done.\r\n")

//save t0~t9,s1~s7
    dli     a2, ARB_STACK_BASE
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   a2, a2, a1
    sd      s0, 0x0(a2)
    sd      s1, 0x8(a2)
    sd      s2, 0x10(a2)
    sd      s3, 0x18(a2)
    sd      s4, 0x20(a2)
    sd      s5, 0x28(a2)
    sd      s6, 0x30(a2)
    sd      s7, 0x38(a2)
    sd      t0, 0x40(a2)
    sd      t1, 0x48(a2)
    sd      t2, 0x50(a2)
    sd      t3, 0x58(a2)
    sd      t4, 0x60(a2)
    sd      t5, 0x68(a2)
    sd      t6, 0x70(a2)
    sd      t7, 0x78(a2)
    sd      t8, 0x80(a2)
    sd      t9, 0x88(a2)

#if 0
    bal     test_mem
    nop
#endif
    PRINTSTR("\r\nStart ARB Leveling....\r\n")

One_ARB_level_begin:
    dli     t8, ARB_STORE_BASE
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   t8, t8, a1
    //initialize to avoid dead loop
    dli     a0, 0x0
    sd      a0, LEVEL_SEQ_BASE(t8)
    //set level sequence--byte 0 mark the end
    dli     a0, 0x0605040302010605
    //dli     a0, 0x0406040306020501
    sd      a0, LEVEL_SEQ_BASE(t8)
    daddiu  t8, t8, 0x8
    dli     a0, 0x0403010605040301
    //dli     a0, 0x0603040604030603
    sd      a0, LEVEL_SEQ_BASE(t8)
    daddiu  t8, t8, 0x8
    dli     a0, 0x000605010605
    //dli     a0, 0x0403060304060403
    sd      a0, LEVEL_SEQ_BASE(t8)
    daddiu  t8, t8, 0x8
    dli     a0, 0x0406040306030406
    sd      a0, LEVEL_SEQ_BASE(t8)
    daddiu  t8, t8, 0x8
    dli     a0, 0x0203040604030603
    sd      a0, LEVEL_SEQ_BASE(t8)
    daddiu  t8, t8, 0x8
    dli     a0, 0
    sd      a0, LEVEL_SEQ_BASE(t8)

    dli     s3, 0x0
#ifdef  DEBUG_ARB_LEVEL
#if 0
    //input init value of t2
	PRINTSTR("\r\nPlease input start value:\r\n");
	bal	    inputaddress
	nop
    dli     a1, 0xff
    and     t2, v0, a1
	PRINTSTR("\r\nPlease input level interval:\r\n");
	bal	    inputaddress
	nop
    dli     a1, 0xff
    and     t5, v0, a1
#else
    dli     t2, 0x0     //trial start addr
    dli     t5, 0x8     //trial step
#endif
#if 0
	PRINTSTR("\r\nPlease input level target:\r\n");
	bal	    inputaddress
	nop
    dli     a1, 0xf
    and     t6, v0, a1
#else
    dli     t6, 0x2     //rdlvl_gate
#endif
#endif

21:
//one level begin
    dli     t8, ARB_STORE_BASE
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   t8, t8, a1
    daddu   a0, t8, s3
    lb      a1, LEVEL_SEQ_BASE(a0)
    move    t6, a1
    beqz    t6, One_ARB_level_end
    nop

    PRINTSTR("\r\nt6 = 0x")
    move    a0, t6
    bal     hexserial
    nop
    PRINTSTR("\r\n")

    dli     t2, 0x0
    dli     t5, 1<<LOG2_STEP 
    //initialize
    dli     t0, 0x1
    dli     t3, 0x0
    dli     t4, 0x0
    dli     t9, 0x0
    //clear store mem
    dli     t8, ARB_STORE_BASE
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   t8, t8, a1
    dli     a0, 0x0
    sd      a0, 0x0(t8)
    sd      a0, 0x8(t8)
    sd      a0, 0x10(t8)
    sd      a0, 0x18(t8)
    sd      a0, 0x20(t8)
    sd      a0, 0x28(t8)
    sd      a0, 0x30(t8)
    sd      a0, 0x38(t8)
    sd      a0, 0x40(t8)
    sd      a0, 0x48(t8)
    sd      a0, 0x50(t8)
    sd      a0, 0x58(t8)
31:
//write new delay value
#include "Modify_param.S"

//do Test and print test result
    bal     test_mem
    nop
    move    t3, v0
    move    t4, v1
#ifdef  DEBUG_ARB_LEVEL
    PRINTSTR("\r\nt2 = 0x")
    move    a0, t2
    bal     hexserial
    nop
    PRINTSTR(":")
#endif
#ifdef  DEBUG_ARB_LEVEL
    beqz    t3, 1f
    nop
    PRINTSTR("\r\nRW Diff 0x")
    dsrl    a0, t3, 32
    bal     hexserial
    nop
    move    a0, t3
    bal     hexserial
    nop
    PRINTSTR("\r\nRD Diff 0x")
    dsrl    a0, t4, 32
    bal     hexserial
    nop
    move    a0, t4
    bal     hexserial
    nop
    b       2f
    nop
1:
    PRINTSTR("\r\nNo Error detected.")
2:
#endif
#if 1
//process TM result: translate Byte error info into 1 bit info in each BX_TM_RST of every Byte.
//64 bit BX_TM_RST work as a bit map corresponding to every param value(so the min step interval
//is 2, or there will be not enough space to store TM RST info), the 64 bit can be only part valid(
//step interval > 2).
#if 0
    //for clk level, use RD Error info
    dli     a0, 0x1
    bne     t6, a0, 1f
    nop
    move    t3, t4
1:
#endif
    dli     t8, ARB_STORE_BASE
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   t8, t8, a1

    dsrl    t7, t3, 56
    and     t7, t7, 0xff
    beqz    t7, 1f
    nop
    //error detected
    ld      a0, B7_TM_RST(t8)
    or      a0, a0, t0
    sd      a0, B7_TM_RST(t8)
1:
    dsrl    t7, t3, 48 
    and     t7, t7, 0xff
    beqz    t7, 1f
    nop
    //error detected
    ld      a0, B6_TM_RST(t8)
    or      a0, a0, t0
    sd      a0, B6_TM_RST(t8)
1:
    dsrl    t7, t3, 40
    and     t7, t7, 0xff
    beqz    t7, 1f
    nop
    //error detected
    ld      a0, B5_TM_RST(t8)
    or      a0, a0, t0
    sd      a0, B5_TM_RST(t8)
1:
    dsrl    t7, t3, 32
    and     t7, t7, 0xff
    beqz    t7, 1f
    nop
    //error detected
    ld      a0, B4_TM_RST(t8)
    or      a0, a0, t0
    sd      a0, B4_TM_RST(t8)
1:
    dsrl    t7, t3, 24
    and     t7, t7, 0xff
    beqz    t7, 1f
    nop
    //error detected
    ld      a0, B3_TM_RST(t8)
    or      a0, a0, t0
    sd      a0, B3_TM_RST(t8)
1:
    dsrl    t7, t3, 16
    and     t7, t7, 0xff
    beqz    t7, 1f
    nop
    //error detected
    ld      a0, B2_TM_RST(t8)
    or      a0, a0, t0
    sd      a0, B2_TM_RST(t8)
1:
    dsrl    t7, t3, 8
    and     t7, t7, 0xff
    beqz    t7, 1f
    nop
    //error detected
    ld      a0, B1_TM_RST(t8)
    or      a0, a0, t0
    sd      a0, B1_TM_RST(t8)
1:
    dsrl    t7, t3, 0
    and     t7, t7, 0xff
    beqz    t7, 1f
    nop
    //error detected
    ld      a0, B0_TM_RST(t8)
    or      a0, a0, t0
    sd      a0, B0_TM_RST(t8)
1:
    dsll    t0, t0, 1
#endif
    //check wether delay value exceed max value
    daddu   a2, t2, t5
    //1 clk max 0x80
    dli     a3, 0x1
    dli     a1, 0x80
    beq     t6, a3, 2f
    nop
    //2 rdlvl_gate max 0x22
    daddiu  a3, a3, 0x1
    dli     a1, 0x22
    beq     t6, a3, 2f
    nop
    //3,4 rdlvl_dqs and rdlvl_dqsn max 0x48
    daddiu  a3, a3, 0x1
    dli     a1, 0x48
    beq     t6, a3, 2f
    nop
    daddiu  a3, a3, 0x1
    beq     t6, a3, 2f
    nop
    //5 wrlvl_dqs max 0x68
    daddiu  a3, a3, 0x1
    dli     a1, 0x68
    beq     t6, a3, 2f
    nop
    //6 wrlvl_dq max 0x40
    daddiu  a3, a3, 0x1
    dli     a1, 0x40
    beq     t6, a3, 2f
    nop
2:
    bgeu    a2, a1, 11f //check the new delay value whether exceed limitation
    nop
    /** not exceed **/
    move    t2, a2
    b       31b
    nop
11: 
#ifdef  DEBUG_ARB_LEVEL
    dli     t8, ARB_STORE_BASE
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   t8, t8, a1
    PRINTSTR("\r\nlevel result is:\r\n")
    ld      t7, B7_TM_RST(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\n")
    ld      t7, B6_TM_RST(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\n")
    ld      t7, B5_TM_RST(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\n")
    ld      t7, B4_TM_RST(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\n")
    ld      t7, B3_TM_RST(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\n")
    ld      t7, B2_TM_RST(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\n")
    ld      t7, B1_TM_RST(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\n")
    ld      t7, B0_TM_RST(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\n")
    PRINTSTR("\r\nt2 = 0x")
    dsrl    a0, t2, 32
    bal     hexserial
    nop
    move    a0, t2
    bal     hexserial
    nop
    PRINTSTR("\r\n")
#endif
#if 1
//calculate mid value for each byte lane
/***********
boundary sign: contain at least WINDOW_ZERO_NUM consecutive 0(TM success)
    t0: parse pointer
    t1: BYTE OFFSET, and work as loop control
    t2: parse max position
    t3: BYTE_X_LEVEL_RST
    t4: WINDOW_ZERO_NUM
***********/
    dli     t1, 0x38    //start from byte 7
    dli     t8, ARB_STORE_BASE
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   t8, t8, a1
    dsrl    t2, t2, LOG2_STEP   //t2 = t2/ 2n
11: //loop for 8 byte lane
    daddu   a1, t8, t1
    ld      t3, B0_TM_RST(a1)
#ifdef  DEBUG_ARB_LEVEL
    PRINTSTR("\r\nt3 = 0x")
    dsrl    a0, t3, 32
    bal     hexserial
    nop
    move    a0, t3
    bal     hexserial
    nop
    PRINTSTR("\r\n")
#endif
    dli     t0, 0x0
    dli     t4, WINDOW_ZERO_NUM
    dli     a0, 0x2
    bne     t6, a0, 9f
    nop
    dsrl    t4, t4, 1   //for rdlvl_gate, use half
9:
12:
    bgtu    t0, t2, 3f
    nop
    dsrl    t7, t3, t0
    and     t7, t7, 0x1
    bnez    t7, 1f
    nop
    //find a TM success
    daddiu  t4, t4, -1
    beqz    t4, 2f
    nop
    //continue move
    daddiu  t0, t0, 0x1
    b       12b
    nop
1:  //find a TM fail
    dli     t4, WINDOW_ZERO_NUM
    dli     a0, 0x2
    bne     t6, a0, 9f
    nop
    dsrl    t4, t4, 1   //for rdlvl_gate, use half
9:
    //continue move
    daddiu  t0, t0, 0x1
    b       12b
    nop
2:  //window found
//<<<<<<<<<<<<<<<
    //calculate the MIN boundary
    dli     a1, WINDOW_ZERO_NUM
#if 1	// skip by Liu Tao
    dli     a0, 0x2
    bne     t6, a0, 9f
    nop
    dsrl    a1, a1, 1   //for rdlvl_gate, use half
#endif
9:
    daddiu  a0, t0, 1
    dsubu   a0, a0, a1
    dsll    a0, a0, LOG2_STEP   //a0 = a0 * 2n
    and     a0, a0, 0xff
    dsll    a0, a0, t1
    ld      a1, GD_MIN(t8)
    or      a1, a1, a0
    sd      a1, GD_MIN(t8)
    //move forward to the next Fail to cal the MAX boundary
1:
    daddiu  t0, t0, 0x1
    bgtu    t0, t2, 1f
    nop
    dsrl    t7, t3, t0
    and     t7, t7, 0x1
    beqz    t7, 1b  //continue move
    nop
1:
    //find a TM FAIL or reach the max test value
    daddiu  a0, t0, -1
    dsll    a0, a0, LOG2_STEP   //a0 = a0 * 2n
    and     a0, a0, 0xff
    dsll    a0, a0, t1
    ld      a1, GD_MAX(t8)
    or      a1, a1, a0
    sd      a1, GD_MAX(t8)
    b       2f
    nop
//>>>>>>>>>>>>>>>>>
3:  //parse to end, CAN NOT find a window
    or      t9, t9, 0x1
    PRINTSTR("Error: This Byte Window not found\r\n")
2:  //continue for next byte lane
    beqz    t1, 2f
    nop
    daddiu  t1, t1, -0x8
    b       11b
    nop
2:  //All byte lane's MIN and MAX value stored or fail to find
    beqz    t9, 1f
    nop
    //some Byte lane can not find a window
    PRINTSTR("\r\nThis level failed.\r\n")
#if 1
    //delay some time and re-level this delay
    dli     a0, 0x40000000
9:
    daddiu  a0, a0, -1
    bnez    a0, 9b
    nop

    b       21b
    nop
#endif
    //write standard value mandatory
    //rdlvl_gate_delay
    dli     a0, 0x0
    dli     a1, 0x2
    beq     t6, a1, 2f
    nop
    //clk delay or wrlvl_delay
    dli     a0, 0x4040404040404040
    dli     a1, 0x1
    beq     t6, a1, 2f
    nop
    dli     a1, 0x5
    beq     t6, a1, 2f
    nop
    //rdlvl_dqsP/N delay or wrlvl_dq delay
    dsrl    a0, a0, 1   //dli     a0, 0x2020202020202020
2:
    sd      a0, GD_MID(t8)
    b       One_Level_Caled
    nop
1:
#if 1
    PRINTSTR("\r\nMin value: 0x")
    ld      t7, GD_MIN(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\nMax value: 0x")
    ld      t7, GD_MAX(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
#endif
//calculate mid value for each byte lane
//seperately take care of clk because 1 clk control multiple SDRAM
    dli     a0, 0x1
    beq     t6, a0, 1f
    nop
    ld      a0, GD_MIN(t8)
    ld      a1, GD_MAX(t8)
    daddu   a0, a0, a1
    //divide a0 by 2 every byte
    dsrl    a0, a0, 1
    dli     a1, 0x7f7f7f7f7f7f7f7f
    and     a0, a0, a1
#if 0
    dli     a2, 0x2
    bne     t6, a2, 2f
    nop
    //rdlvl gate use (min + mid value) / 2.
    ld      a1, GD_MIN(t8)
    daddu   a0, a0, a1
    dsrl    a0, a0, 1
    dli     a1, 0x7f7f7f7f7f7f7f7f
    and     a0, a0, a1
2:
#endif
    //add some offset for wrlvl_dq
    dli	    a1, 0x6
    bne	    t6, a1, 2f
    nop
    dli     a2, 0x0202020202020202
    //daddu   a0, a0, a2	// skip by Liu Tao
2:
    sd      a0, GD_MID(t8)
    b       2f
    nop
1:  //calculate clk mid point
    //clk 2--start
    ld      a0, GD_MIN(t8)
    dsrl    a1, a0, 0x38
    and     a1, a1, 0xff
    dsrl    a2, a0, 0x30
    and     a2, a2, 0xff
    //a1=MAX(a1, a2)
    bgtu    a1, a2, 1f
    nop
    move    a1, a2
1:
    dsrl    a2, a0, 0x28
    and     a2, a2, 0xff
    //a1=MAX(a1, a2)
    bgtu    a1, a2, 1f
    nop
    move    a1, a2
1:  
    move    t7, a1
    
    ld      a0, GD_MAX(t8)
    dsrl    a1, a0, 0x38
    and     a1, a1, 0xff
    dsrl    a2, a0, 0x30
    and     a2, a2, 0xff
    //a1=MIN(a1, a2)
    bltu    a1, a2, 1f
    nop
    move    a1, a2
1:
    dsrl    a2, a0, 0x28
    and     a2, a2, 0xff
    //a1=MIN(a1, a2)
    bltu    a1, a2, 1f
    nop
    move    a1, a2
1:  
    daddu   t7, t7, a1
    dsrl    t7, t7, 1   //t7=t7/2
    dsll    t7, t7, 0x28
    ld      a1, GD_MID(t8)
    or      a1, a1, t7
    sd      a1, GD_MID(t8)
    //clk 2--end
    //clk 1--start
    ld      a0, GD_MIN(t8)
    dsrl    a1, a0, 0x20
    and     a1, a1, 0xff
    dsrl    a2, a0, 0x18
    and     a2, a2, 0xff
    //a1=MAX(a1, a2)
    bgtu    a1, a2, 1f
    nop
    move    a1, a2
1:
    move    t7, a1
    
    ld      a0, GD_MAX(t8)
    dsrl    a1, a0, 0x20
    and     a1, a1, 0xff
    dsrl    a2, a0, 0x18
    and     a2, a2, 0xff
    //a1=MIN(a1, a2)
    bltu    a1, a2, 1f
    nop
    move    a1, a2
1:
    daddu   t7, t7, a1
    dsrl    t7, t7, 1
    dsll    t7, t7, 0x18
    ld      a1, GD_MID(t8)
    or      a1, a1, t7
    sd      a1, GD_MID(t8)
    //clk 1--end
    //clk 0--start
    ld      a0, GD_MIN(t8)
    dsrl    a1, a0, 0x10
    and     a1, a1, 0xff
    dsrl    a2, a0, 0x8
    and     a2, a2, 0xff
    //a1=MAX(a1, a2)
    bgtu    a1, a2, 1f
    nop
    move    a1, a2
1:
    dsrl    a2, a0, 0x0
    and     a2, a2, 0xff
    //a1=MAX(a1, a2)
    bgtu    a1, a2, 1f
    nop
    move    a1, a2
1:  
    move    t7, a1
    
    ld      a0, GD_MAX(t8)
    dsrl    a1, a0, 0x10
    and     a1, a1, 0xff
    dsrl    a2, a0, 0x8
    and     a2, a2, 0xff
    //a1=MIN(a1, a2)
    bltu    a1, a2, 1f
    nop
    move    a1, a2
1:
    dsrl    a2, a0, 0x0
    and     a2, a2, 0xff
    //a1=MIN(a1, a2)
    bltu    a1, a2, 1f
    nop
    move    a1, a2
1:  
    daddu   t7, t7, a1
    dsrl    t7, t7, 1
    dsll    t7, t7, 0x0
    ld      a1, GD_MID(t8)
    or      a1, a1, t7
    sd      a1, GD_MID(t8)
    //clk 0--end
2:
#if 1
    PRINTSTR("\r\nCal Mid value: 0x")
    ld      t7, GD_MID(t8)
    dsrl    a0, t7, 32
    bal     hexserial
    nop
    move    a0, t7
    bal     hexserial
    nop
    PRINTSTR("\r\n")
#endif
One_Level_Caled:
    ld      t2, GD_MID(t8)
#ifdef DEBUG_ARB_LEVEL
    PRINTSTR("\r\nWrite param and value:\r\nt6 = 0x")
    dsrl    a0, t6, 32
    bal     hexserial
    nop
    move    a0, t6
    bal     hexserial
    nop
    PRINTSTR("\r\nt2 = 0x")
    dsrl    a0, t2, 32
    bal     hexserial
    nop
    move    a0, t2
    bal     hexserial
    nop
    PRINTSTR("\r\n")
#endif
#include    "Write_Leveled_param.S"
#endif

    //move to next level
    daddiu  s3, s3, 0x1
    b       21b
    nop

One_ARB_level_end:
    bal     test_mem
    nop
    beqz    v0, 2f
    nop
    //level fail, delay some time and level again
    dli     a0, 0x4000000
1:
    daddiu  a0, a0, -1
    bnez    a0, 1b
    nop
    b       One_ARB_level_begin
    nop

2:
    PRINTSTR("\r\nARB Leveling Finished.\r\n")
    b       ARB_level_end
    nop
    
#include "ARB_TM_Q.S"
ARB_level_end:
    //print final level result
	PRINTSTR("\r\nEnable DDR MC config space.\r\n");
    dli     t7, 0x900000001fe00180
    lw      a1, 0x0(t7)
    li      a0, 0xfffffeff
    and     a1, a1, a0
    sw      a1, 0x0(t7)
    sync

#if 1
    PRINTSTR("The leveled MC configuration is:\r\n")
#ifdef  loongson3A3
    dli     t1, 180
#else
    dli     t1, 152
#endif
    dli     a0, ARB_LEVEL_NODE_ID
    dsll    a0, a0, 44
    dli     t5, 0x900000000ff00000
    or      t5, t5, a0
1:
    ld      t6, 0x0(t5)
    dsrl    a0, t6, 32
    bal	    hexserial
    nop
    PRINTSTR("  ")
    move    a0, t6
    bal	    hexserial
    nop
    PRINTSTR("\r\n")

    daddiu  t1, t1, -1
    daddiu  t5, t5, 16
    bnez    t1, 1b
    nop
#endif

    //disable DDR MC config reg space
    dli     t7, 0x900000001fe00180
    lw      a1, 0x0(t7)
    or      a1, a1, 0x100
    sw      a1, 0x0(t7)
	PRINTSTR("\r\nDisable DDR MC config space.\r\n");

//recover t0~t9,s1~s7
    dli     a2, ARB_STACK_BASE
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   a2, a2, a1
    ld      s0, 0x0(a2)
    ld      s1, 0x8(a2)
    ld      s2, 0x10(a2)
    ld      s3, 0x18(a2)
    ld      s4, 0x20(a2)
    ld      s5, 0x28(a2)
    ld      s6, 0x30(a2)
    ld      s7, 0x38(a2)
    ld      t0, 0x40(a2)
    ld      t1, 0x48(a2)
    ld      t2, 0x50(a2)
    ld      t3, 0x58(a2)
    ld      t4, 0x60(a2)
    ld      t5, 0x68(a2)
    ld      t6, 0x70(a2)
    ld      t7, 0x78(a2)
    ld      t8, 0x80(a2)
    ld      t9, 0x88(a2)

/*
 *Unlock Scache 9800?00000000000 ~ 9800?00000001000(4K)
 */
    PRINTSTR("\r\nUnlock Scache Node x--9800?00000000000~4K...\r\n")
    dli     a2, 0x900000003ff00200
    dli     a1, ARB_LEVEL_NODE_ID
    dsll    a1, a1, 44
    daddu   a2, a2, a1
    dli     a3, 0x0000000000000000
    sd      a3, 0x0(a2)
    PRINTSTR("Unlock Scache Done.\r\n")

    move    ra, t9
    jr      ra
    nop
