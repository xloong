/*****************************
    Macro defination for DDR MC parameters
    Author: Chen Xinke
    v0.1    
*******************************/
#define DDR_MC_CONFIG_BASE      0x900000000ff00000
#define MC_CONFIG_REG_BASE_ADDR 0x900000000ff00000

#define START_ADDR              (0x30)
#define INT_STATUS_ADDR         (0x960)
#define RESYNC_DLL_ADDR         (0x980)
#define START_OFFSET            40
#define INIT_COMPLETE_OFFSET    8
#define RESYNC_DLL_OFFSET       16

#define CLKLVL_DELAY_2_ADDR      (0x8f0)
#define CLKLVL_DELAY_1_ADDR      (0x8f0)
#define CLKLVL_DELAY_0_ADDR      (0x8f0)
#define CLKLVL_DELAY_2_OFFSET    24
#define CLKLVL_DELAY_1_OFFSET    16
#define CLKLVL_DELAY_0_OFFSET    8

#define PAD_OUTPUT_WINDOW_8_ADDR      (0x310)
#define PAD_OUTPUT_WINDOW_7_ADDR      (0x310)
#define PAD_OUTPUT_WINDOW_6_ADDR      (0x300)
#define PAD_OUTPUT_WINDOW_5_ADDR      (0x300)
#define PAD_OUTPUT_WINDOW_4_ADDR      (0x2f0)
#define PAD_OUTPUT_WINDOW_3_ADDR      (0x2f0)
#define PAD_OUTPUT_WINDOW_2_ADDR      (0x2e0)
#define PAD_OUTPUT_WINDOW_1_ADDR      (0x2e0)
#define PAD_OUTPUT_WINDOW_0_ADDR      (0x2d0)
#define PAD_OUTPUT_WINDOW_8_OFFSET    32
#define PAD_OUTPUT_WINDOW_7_OFFSET    0
#define PAD_OUTPUT_WINDOW_6_OFFSET    32
#define PAD_OUTPUT_WINDOW_5_OFFSET    0
#define PAD_OUTPUT_WINDOW_4_OFFSET    32
#define PAD_OUTPUT_WINDOW_3_OFFSET    0
#define PAD_OUTPUT_WINDOW_2_OFFSET    32
#define PAD_OUTPUT_WINDOW_1_OFFSET    0
#define PAD_OUTPUT_WINDOW_0_OFFSET    32
#define PAD_OUTPUT_WINDOW_MASK     (0xffff)

#define PHY_CTRL_0_8_ADDR        (0x310)
#define PHY_CTRL_0_7_ADDR        (0x310)
#define PHY_CTRL_0_6_ADDR        (0x300)
#define PHY_CTRL_0_5_ADDR        (0x300)
#define PHY_CTRL_0_4_ADDR        (0x2f0)
#define PHY_CTRL_0_3_ADDR        (0x2f0)
#define PHY_CTRL_0_2_ADDR        (0x2e0)
#define PHY_CTRL_0_1_ADDR        (0x2e0)
#define PHY_CTRL_0_0_ADDR        (0x2d0)
#define PHY_CTRL_0_8_OFFSET      32
#define PHY_CTRL_0_7_OFFSET      0
#define PHY_CTRL_0_6_OFFSET      32
#define PHY_CTRL_0_5_OFFSET      0
#define PHY_CTRL_0_4_OFFSET      32
#define PHY_CTRL_0_3_OFFSET      0
#define PHY_CTRL_0_2_OFFSET      32
#define PHY_CTRL_0_1_OFFSET      0
#define PHY_CTRL_0_0_OFFSET      32
#define PHY_CTRL_0_MASK          (0xffffffff)

#define PHY_CTRL_1_8_ADDR        (0x360)
#define PHY_CTRL_1_7_ADDR        (0x350)
#define PHY_CTRL_1_6_ADDR        (0x350)
#define PHY_CTRL_1_5_ADDR        (0x340)
#define PHY_CTRL_1_4_ADDR        (0x340)
#define PHY_CTRL_1_3_ADDR        (0x330)
#define PHY_CTRL_1_2_ADDR        (0x330)
#define PHY_CTRL_1_1_ADDR        (0x320)
#define PHY_CTRL_1_0_ADDR        (0x320)
#define PHY_CTRL_1_8_OFFSET      0
#define PHY_CTRL_1_7_OFFSET      32
#define PHY_CTRL_1_6_OFFSET      0
#define PHY_CTRL_1_5_OFFSET      32
#define PHY_CTRL_1_4_OFFSET      0
#define PHY_CTRL_1_3_OFFSET      32
#define PHY_CTRL_1_2_OFFSET      0
#define PHY_CTRL_1_1_OFFSET      32
#define PHY_CTRL_1_0_OFFSET      0 
#define PHY_CTRL_1_MASK          (0xffffffff)

#define WRLVL_DQ_DELAY_8_ADDR      (0x230)
#define WRLVL_DQ_DELAY_7_ADDR      (0x230)
#define WRLVL_DQ_DELAY_6_ADDR      (0x220)
#define WRLVL_DQ_DELAY_5_ADDR      (0x220)
#define WRLVL_DQ_DELAY_4_ADDR      (0x210)
#define WRLVL_DQ_DELAY_3_ADDR      (0x210)
#define WRLVL_DQ_DELAY_2_ADDR      (0x200)
#define WRLVL_DQ_DELAY_1_ADDR      (0x200)
#define WRLVL_DQ_DELAY_0_ADDR      (0x1f0)
#define WRLVL_DQ_DELAY_8_OFFSET    48
#define WRLVL_DQ_DELAY_7_OFFSET    16
#define WRLVL_DQ_DELAY_6_OFFSET    48
#define WRLVL_DQ_DELAY_5_OFFSET    16
#define WRLVL_DQ_DELAY_4_OFFSET    48
#define WRLVL_DQ_DELAY_3_OFFSET    16
#define WRLVL_DQ_DELAY_2_OFFSET    48
#define WRLVL_DQ_DELAY_1_OFFSET    16
#define WRLVL_DQ_DELAY_0_OFFSET    48

#define RDLVL_DQSN_DELAY_8_ADDR      (0x280)
#define RDLVL_DQSN_DELAY_7_ADDR      (0x270)
#define RDLVL_DQSN_DELAY_6_ADDR      (0x270)
#define RDLVL_DQSN_DELAY_5_ADDR      (0x260)
#define RDLVL_DQSN_DELAY_4_ADDR      (0x260)
#define RDLVL_DQSN_DELAY_3_ADDR      (0x250)
#define RDLVL_DQSN_DELAY_2_ADDR      (0x250)
#define RDLVL_DQSN_DELAY_1_ADDR      (0x240)
#define RDLVL_DQSN_DELAY_0_ADDR      (0x240)
#define RDLVL_DQSN_DELAY_8_OFFSET    8
#define RDLVL_DQSN_DELAY_7_OFFSET    40
#define RDLVL_DQSN_DELAY_6_OFFSET    8
#define RDLVL_DQSN_DELAY_5_OFFSET    40
#define RDLVL_DQSN_DELAY_4_OFFSET    8
#define RDLVL_DQSN_DELAY_3_OFFSET    40
#define RDLVL_DQSN_DELAY_2_OFFSET    8
#define RDLVL_DQSN_DELAY_1_OFFSET    40
#define RDLVL_DQSN_DELAY_0_OFFSET    8

#ifdef  loongson3A3
#define CLKLVL_DELAY_MASK       (0xff)
#define RDLVL_GATE_DELAY_MASK   (0xffff)
#define RDLVL_DELAY_MASK        (0xffff)
#define RDLVL_DQSN_DELAY_MASK   (0xffff)
#define WRLVL_DELAY_MASK        (0xffff)
#define WRLVL_DQ_DELAY_MASK     (0xffff)

#define WRLVL_DELAY_8_ADDR      (0xb10)
#define WRLVL_DELAY_7_ADDR      (0xb10)
#define WRLVL_DELAY_6_ADDR      (0xb10)
#define WRLVL_DELAY_5_ADDR      (0xb00)
#define WRLVL_DELAY_4_ADDR      (0xb00)
#define WRLVL_DELAY_3_ADDR      (0xb00)
#define WRLVL_DELAY_2_ADDR      (0xb00)
#define WRLVL_DELAY_1_ADDR      (0xaf0)
#define WRLVL_DELAY_0_ADDR      (0xaf0)
#define WRLVL_DELAY_8_OFFSET    32
#define WRLVL_DELAY_7_OFFSET    16
#define WRLVL_DELAY_6_OFFSET    0
#define WRLVL_DELAY_5_OFFSET    48
#define WRLVL_DELAY_4_OFFSET    32
#define WRLVL_DELAY_3_OFFSET    16
#define WRLVL_DELAY_2_OFFSET    0
#define WRLVL_DELAY_1_OFFSET    48
#define WRLVL_DELAY_0_OFFSET    32

#define RDLVL_GATE_DELAY_8_ADDR (0xa90)
#define RDLVL_GATE_DELAY_7_ADDR (0xa90)
#define RDLVL_GATE_DELAY_6_ADDR (0xa90)
#define RDLVL_GATE_DELAY_5_ADDR (0xa90)
#define RDLVL_GATE_DELAY_4_ADDR (0xa80)
#define RDLVL_GATE_DELAY_3_ADDR (0xa80)
#define RDLVL_GATE_DELAY_2_ADDR (0xa80)
#define RDLVL_GATE_DELAY_1_ADDR (0xa80)
#define RDLVL_GATE_DELAY_0_ADDR (0xa70)
#define RDLVL_GATE_DELAY_8_OFFSET    48
#define RDLVL_GATE_DELAY_7_OFFSET    32
#define RDLVL_GATE_DELAY_6_OFFSET    16
#define RDLVL_GATE_DELAY_5_OFFSET    0
#define RDLVL_GATE_DELAY_4_OFFSET    48
#define RDLVL_GATE_DELAY_3_OFFSET    32
#define RDLVL_GATE_DELAY_2_OFFSET    16
#define RDLVL_GATE_DELAY_1_OFFSET    0
#define RDLVL_GATE_DELAY_0_OFFSET    48

#define RDLVL_DELAY_8_ADDR      (0xa50)
#define RDLVL_DELAY_7_ADDR      (0xa50)
#define RDLVL_DELAY_6_ADDR      (0xa40)
#define RDLVL_DELAY_5_ADDR      (0xa40)
#define RDLVL_DELAY_4_ADDR      (0xa40)
#define RDLVL_DELAY_3_ADDR      (0xa40)
#define RDLVL_DELAY_2_ADDR      (0xa30)
#define RDLVL_DELAY_1_ADDR      (0xa30)
#define RDLVL_DELAY_0_ADDR      (0xa30)
#define RDLVL_DELAY_8_OFFSET    16
#define RDLVL_DELAY_7_OFFSET    0
#define RDLVL_DELAY_6_OFFSET    48
#define RDLVL_DELAY_5_OFFSET    32
#define RDLVL_DELAY_4_OFFSET    16
#define RDLVL_DELAY_3_OFFSET    0
#define RDLVL_DELAY_2_OFFSET    48
#define RDLVL_DELAY_1_OFFSET    32
#define RDLVL_DELAY_0_OFFSET    16
#else
#define CLKLVL_DELAY_MASK       (0xff)
#define RDLVL_GATE_DELAY_MASK   (0xff)
#define RDLVL_DELAY_MASK        (0xff)
#define RDLVL_DQSN_DELAY_MASK   (0xff)
#define WRLVL_DELAY_MASK        (0xff)
#define WRLVL_DQ_DELAY_MASK     (0xff)

#define WRLVL_DELAY_8_ADDR      (0x840)
#define WRLVL_DELAY_7_ADDR      (0x840)
#define WRLVL_DELAY_6_ADDR      (0x840)
#define WRLVL_DELAY_5_ADDR      (0x830)
#define WRLVL_DELAY_4_ADDR      (0x830)
#define WRLVL_DELAY_3_ADDR      (0x830)
#define WRLVL_DELAY_2_ADDR      (0x830)
#define WRLVL_DELAY_1_ADDR      (0x830)
#define WRLVL_DELAY_0_ADDR      (0x830)
#define WRLVL_DELAY_8_OFFSET    16
#define WRLVL_DELAY_7_OFFSET    8
#define WRLVL_DELAY_6_OFFSET    0
#define WRLVL_DELAY_5_OFFSET    56
#define WRLVL_DELAY_4_OFFSET    48
#define WRLVL_DELAY_3_OFFSET    40
#define WRLVL_DELAY_2_OFFSET    32
#define WRLVL_DELAY_1_OFFSET    24
#define WRLVL_DELAY_0_OFFSET    16

#define RDLVL_GATE_DELAY_8_ADDR     (0x7f0)
#define RDLVL_GATE_DELAY_7_ADDR     (0x7f0)
#define RDLVL_GATE_DELAY_6_ADDR     (0x7f0)
#define RDLVL_GATE_DELAY_5_ADDR     (0x7f0)
#define RDLVL_GATE_DELAY_4_ADDR     (0x7f0)
#define RDLVL_GATE_DELAY_3_ADDR     (0x7f0)
#define RDLVL_GATE_DELAY_2_ADDR     (0x7e0)
#define RDLVL_GATE_DELAY_1_ADDR     (0x7e0)
#define RDLVL_GATE_DELAY_0_ADDR     (0x7e0)
#define RDLVL_GATE_DELAY_8_OFFSET    40
#define RDLVL_GATE_DELAY_7_OFFSET    32
#define RDLVL_GATE_DELAY_6_OFFSET    24
#define RDLVL_GATE_DELAY_5_OFFSET    16
#define RDLVL_GATE_DELAY_4_OFFSET    8
#define RDLVL_GATE_DELAY_3_OFFSET    0
#define RDLVL_GATE_DELAY_2_OFFSET    56
#define RDLVL_GATE_DELAY_1_OFFSET    48
#define RDLVL_GATE_DELAY_0_OFFSET    40

#define RDLVL_DELAY_8_ADDR      (0x230)
#define RDLVL_DELAY_7_ADDR      (0x230)
#define RDLVL_DELAY_6_ADDR      (0x220)
#define RDLVL_DELAY_5_ADDR      (0x220)
#define RDLVL_DELAY_4_ADDR      (0x210)
#define RDLVL_DELAY_3_ADDR      (0x210)
#define RDLVL_DELAY_2_ADDR      (0x200)
#define RDLVL_DELAY_1_ADDR      (0x200)
#define RDLVL_DELAY_0_ADDR      (0x1f0)
#define RDLVL_DELAY_8_OFFSET    40
#define RDLVL_DELAY_7_OFFSET    8
#define RDLVL_DELAY_6_OFFSET    40
#define RDLVL_DELAY_5_OFFSET    8
#define RDLVL_DELAY_4_OFFSET    40
#define RDLVL_DELAY_3_OFFSET    8
#define RDLVL_DELAY_2_OFFSET    40
#define RDLVL_DELAY_1_OFFSET    8
#define RDLVL_DELAY_0_OFFSET    40
#endif

//------------------------
//define for ddr configure register param location
#define EIGHT_BANK_MODE_ADDR     (0x10)
#define COLUMN_SIZE_ADDR         (0x50)
#define ADDR_PINS_ADDR           (0x50)
#define CS_MAP_ADDR              (0x70)
#define REDUC_ADDR               (0x30)
#define CTRL_RAW_ADDR            (0x40)
#define EIGHT_BANK_MODE_OFFSET  32
#define COLUMN_SIZE_OFFSET      24
#define ADDR_PINS_OFFSET        8
#define CS_MAP_OFFSET           16
#define REDUC_OFFSET            8
#define CTRL_RAW_OFFSET         56
//------------------------

//------------------------------------
//for ddr3_level.S
#define SWLVL_RESP_8_ADDR       (0x7b0)
#define SWLVL_RESP_7_ADDR       (0x7b0)
#define SWLVL_RESP_6_ADDR       (0x7a0)
#define SWLVL_RESP_5_ADDR       (0x7a0)
#define SWLVL_RESP_4_ADDR       (0x7a0)
#define SWLVL_RESP_3_ADDR       (0x7a0)
#define SWLVL_RESP_2_ADDR       (0x7a0)
#define SWLVL_RESP_1_ADDR       (0x7a0)
#define SWLVL_RESP_0_ADDR       (0x7a0)

#define SWLVL_START_ADDR        (0x960)
#define SWLVL_START_OFFSET      40
#define SWLVL_LOAD_ADDR         (0x960)
#define SWLVL_LOAD_OFFSET       32
#define SWLVL_EXIT_ADDR         (0x960)
#define SWLVL_EXIT_OFFSET       24
#define SW_LEVELING_MODE_ADDR   (0x750)
#define SW_LEVELING_MODE_OFFSET 48
#define SWLVL_OP_DONE_ADDR      (0x720)
#define SWLVL_OP_DONE_OFFSET    8

#ifdef  loongson3A3
#define WRLVL_EN_ADDR           (0x980)
#define WRLVL_EN_OFFSET         32
#define WRLVL_REG_EN_ADDR       (0x980)
#define WRLVL_REG_EN_OFFSET     40
#define RDLVL_GATE_REG_EN_ADDR  (0x980)
#define RDLVL_GATE_REG_EN_OFFSET 0
#define RDLVL_REG_EN_ADDR       (0x980)
#define RDLVL_REG_EN_OFFSET     8
#else
#define WRLVL_EN_ADDR           (0x710)
#define WRLVL_EN_OFFSET         48
#endif
#define WRLVL_CS_ADDR           (0x750)
#define WRLVL_CS_OFFSET         56
#define RDLVL_GATE_EN_ADDR      (0x720)
#define RDLVL_GATE_EN_OFFSET    40
#define RDLVL_EN_ADDR           (0x720)
#define RDLVL_EN_OFFSET         32
#define RDLVL_CS_ADDR           (0x750)
#define RDLVL_CS_OFFSET         40
#define RDLVL_EDGE_ADDR         (0x940)
#define RDLVL_EDGE_OFFSET       24
