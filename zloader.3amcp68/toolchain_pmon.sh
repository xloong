#!/bin/bash - 
#===============================================================================
#
#          FILE:  toolchain_pmon.sh
# 
#         USAGE:  ./toolchain_pmon.sh 
# 
#   DESCRIPTION:  Add toolchain for PMON
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  JiangTao (JT), jiangtao@ncic.ac.cn
#       COMPANY:  CAR Group, NCIC,ICT,CAS.
#       VERSION:  1.0
#       CREATED:  2010年01月22日 20时31分36秒 CST
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
export PATH=/usr/local/comp/mips-elf/gcc-2.95.3/bin:$PATH
bash


